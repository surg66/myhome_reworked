--- EN ---
All players become the owners of their walls.
In MyHome mode, the walls are invulnerable, and the gates can only be used by owners and friends. 
MyHome protects against griefers. 
Comming bosses, aggressive mobs pushes out of MyHome. (optional) 
The bag of klaus, the mobs that are being hunted, are pushed out of MyHome. (optional)
During a lunar storm, Wagstaff cannot enter MyHome. 
Forbidden to build MyHome in the ruins, atrium, archive and on the hermit island. (optional) 
Important objects in the world: fragments of statues, altars, sphere, guardian key, etc. will push out of MyHome. 

Building MyHome:
1) Right-click on the wall, which will be the start wall.
2) Left-click on a highlighted wall with a marker to set direction. 
3) If looped walls, MyHome will be build. 

Removing MyHome by right-clicking on the start wall.

Add friend: Press U and write #add(№ player) 
Delete friend: Press U and write #del(№ player) 
The player number can be found by pressing TAB. For example: #add5 

An alternative way to add/delete friends is available in a special interface, 
opens by "Esc" -> "Menu pause" and by clicking on the "MyHome" button.

The controller is supported. 

IMPORTANT: this mod is not compatible with the original MyHome and is not interchangeable, install only if you are generate new world. 

MyHome Rework this is new code, but keeping the idea of the original MyHome. 

Why did I rewrite MyHome?
1) The original is not supported by the author and is outdated, sometimes crashes.
2) All calculations took place only around the radius of the wall and your house did not really know its shape, now you can build your house of any shape and it will be taken into account when calculating, for example, pushing the bag of Klaus out.
3) Fixing minor bugs of the original, and support mod with Klei updates.

Original MyHome.
https://steamcommunity.com/sharedfiles/filedetails/?id=796742922

MyHome(modified), this is my modified version of the original, whose support has been STOPPED.
https://steamcommunity.com/sharedfiles/filedetails/?id=1836259307

Big thanks Ambassador for donate.
Thanks for testing: Magic, Veria, Wendy, Yalinka, Commander, ДОРИ, (Ken)Владимир.
Thanks for translate: 琪莎猫 (Chinese); DienMarion, G-H community (VietNam), DishierCloud188(Spanish)

P.S. If you are a native speaker, you can help translate this mod into your language.

--- SCH ---
MyHome Reworked(私宅-重做)

所有玩家将成为他们自己建造墙体的所有者。
在私宅模式下，私宅所使用的墙体将会无敌，门则只有所有者和其好友列表中的好友能使用。
私宅可以保护你避免一些糟心事。
袭击BOSS，袭击生物将被赶出私宅。(可选)
克劳斯的赃物袋，被追捕的生物将被赶出私宅。(可选)
月亮风暴期间，瓦史塔夫(发明家)不能进入私宅。
禁止在废墟区域、中庭区域、档案馆区域，寄居蟹隐士岛建立私宅。(可选)
世界中的重要物品：雕像、底座、宝球、远古钥匙等物品将会被从私宅中推出去。

建立私宅：
1)使用鼠标右键在墙体上点击，该墙体将作为起始墙体。
2)在带有标记的高亮显示墙体上点击鼠标左键以设置方向。
3)如果形成环状墙体，私宅就会被建立。

鼠标右键点击墙体可撤除私宅。

添加好友：按U，输入 #add(玩家编号)
删除好友：按U，输入 #del(玩家编号)
按TAB键可在最左侧看到对应的玩家编号，例如 #add5

添加/删除好友的另一种方式是在一个特殊界面中，
按ESC键，找到“私宅”按钮，点击打开。

支持手柄。

重要说明：该MOD与原版MyHome并不兼容，并不可交替使用，仅在你生成新世界时安装。

MyHome Rework是重写的新代码，但是维持了原版MyHome的理念。

为什么我要重写MyHome？
1)原版作者已经不支持，并且版本过时，有时还会崩溃。
2)所有计算都是围绕墙体的半径进行，并且你的房子并不真正知道它的形状。但现在你可以建造任何形状的房子，在计算时将会将这些直接纳入考虑，例如，将克劳斯的赃物袋推出去。
3)修复了不少原版的Bug，并且支持跟随Klei更新MOD。

MyHome(Original)，原版MyHome
https://steamcommunity.com/sharedfiles/filedetails/?id=796742922

MyHome(modified)，这是我对原版的修改版本，已经停止支持
https://steamcommunity.com/sharedfiles/filedetails/?id=1836259307

非常感谢Ambassador的捐赠。
感谢测试人员: Magic, Veria, Wendy, Yalinka, Commander, ДОРИ, (Ken)Владимир.
感谢翻译人员: 琪莎猫 (Chinese); DienMarion, G-H community (VietNam), DishierCloud188(Spanish)

P.S. 如果你十分擅长自己的母语，你可以提供帮助以将该MOD翻译成你使用的语言。

--- RU ---
Все игроки становятся владельцами своих стен.
В режиме MyHome - стены неуязвимы, а ворота могут использовать только владельцы и друзья.
MyHome защищает от гриферов.
Нападающих боссов, мобов и т.д. выталкивает из MyHome. (опционально)
Мешок клауса, мобов за которыми ведётся охота, выталкивает из MyHome. (опционально)
Во время лунной бури Вагстаф не может зайти в MyHome.
Запрещено строить MyHome в руинах, атриуме, архиве и на острове отшельницы. (опционально)
Важные объекты мира: фрагменты статуй, небесные алтари, небесная сфера, жемчужина, ключ стража и т.д. будет выталкивать из MyHome.

Установка MyHome:
1) Правый клик по стене, которая станет началом установки.
2) Левый клик по подсвеченной стене, выделенной маркером для указания направления.
3) При зацикленном контуре стен, будет выполнена установка MyHome

Удаление MyHome правым кликом по стене, которая была началом установки.

Добавить друга: Нажмите U и напишите #add(№ игрока)
Удалить из друзей: Нажмите U и напишите #del(№ игрока)
Номер игрока можно узнать, нажав TAB. Пример: #add5

Альтернативный способ добавления/удаления друзей доступен в специальном интерфейсе,
открывается по "Esc"->"Menu pause" и кликом по кнопке "MyHome".

Контроллер поддерживается.

ВАЖНО: этот мод не совместим с оригинальным MyHome и не взаимозаменяем, устанавливайте только если генерируете новый мир.

MyHome Rework это переписанный заново код, но с сохранением идеи оригинального MyHome.

Почему я переписал MyHome?
1) Оригинал не поддерживается автором и он устарел, иногда крашится.
2) Все расчёты происходили только вокруг радиуса стены и ваш дом на самом деле не знал своей формы, теперь вы можете делать ваш дом любой формы и она будет учитываться при расчётах, например выталкивание мешка Клауса наружу.
3) Исправление мелких багов оригинала, и поддержка в актуальном состоянии с обновлениями Klei.

Основные отличия от MyHome(modified):
+ Полностью переписанный код, изменение структуры данных, поэтому несовместим с оригинальным MyHome.
+ Теперь вы можете строить/удалять MyHome независимо от других игроков (раньше манипуляции с MyHome блокировались для одного игрока в мире).
+ Улучшена индикация подсказок при строительстве.
+ Теперь при строительстве MyHome недостаточно навести на выделенную стену, нужно на неё ещё и кликнуть (раньше можно было случайно навести на другую стену, которую не хотел).
+ Специальный интерфейс даёт возможность добавлять/удалять друзей, даже если они в другом мире или в offline (чтобы попасть в список, нужно быть в online, либо вас кто-то добавил, либо построить хотя бы 1 стену в мире).
+ Больше нет проблем со входом внутрь MyHome, у которого множество хозяев стен, достаточно дружить с его хозяином (хозяин MyHome, а не стен).
+ Структура MyHome теперь знает свою форму, поэтому открываются новые возможности :)
+ Улучшено выталкивание из MyHome объектов.
+ Важные объекты мира будут вытолкнуты из MyHome при попытке их туда занести (например: небесная сфера, жемчужина, древний ключ, части статуй для теневых шахмат, части небесного святилища, части небесного алтаря, небесное подношение и т.д.).
+ Если в момент постройки MyHome внутри окажутся зарытые (невидимые) части для небесного святилища, их вытолкнет наружу зарытыми (раньше могли остаться внутри MyHome).
+ Улучшено выталкивание игроков из MyHome (intruders: "get out"), теперь игроков вытолкнет только если игрок забежал внутрь, например в открытую дверь.
+ Улучшена навигация Вагстаффа в лунную бурю, теперь он обходит закрытый MyHome (были случаи, при которых он стремился подбежать к игроку, который внутри MyHome).
+ Если следующий след от животного, за которым ведётся охота, появится внутри MyHome, его вытолкнет и последний след будет повёрнут в нужном направлении (раньше направление не изменялось).
+ Теневые двойники Максвелла игнорируют работу, которую запрещено делать рядом/внутри чужого MyHome (раньше подбегали к деревьям, пенькам, камням и т.д. в попытке сделать работу).
+ Добавлена поддержка конроллера.
+ Улучшены/исправлены всякие мелочи, которые вы даже не знали или не замечали :)

Original MyHome.
https://steamcommunity.com/sharedfiles/filedetails/?id=796742922

MyHome(modified), это моя модифицированная версия оригинала, поддержка которого ОСТАНОВЛЕНА.
https://steamcommunity.com/sharedfiles/filedetails/?id=1836259307

Большое спасибо Ambassador за пожертвование.
Спасибо тестировщикам: Magic, Veria, Wendy, Yalinka, Commander, ДОРИ, (Ken)Владимир.
Спасибо переводчикам: 琪莎猫 (Chinese); DienMarion, G-H сообщество(VietNam), DishierCloud188(Spanish)

P.S. Если вы являетесь носителем языка, вы можете помочь перевести мод на ваш язык.
