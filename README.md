# Desctiption
MyHome reworked - Don't Starve Together modification.

All players become the owners of their walls.
In MyHome mode, the walls are invulnerable, and the gates can only be used by owners and friends. 
MyHome protects against griefers. 
Comming bosses, aggressive mobs pushes out of MyHome. (optional) 
The bag of klaus, the mobs that are being hunted, are pushed out of MyHome. (optional)
During a lunar storm, Wagstaff cannot enter MyHome. 
Forbidden to build MyHome in the ruins, atrium, archive and on the hermit island. (optional) 
Important objects in the world: fragments of statues, altars, sphere, guardian key, etc. will push out of MyHome. 

Building MyHome:
1) Right-click on the wall, which will be the start wall.
2) Left-click on a highlighted wall with a marker to set direction. 
3) If looped walls, MyHome will be build. 

Removing MyHome by right-clicking on the start wall.

Add friend: Press U and write #add(№ player) 
Delete friend: Press U and write #del(№ player) 
The player number can be found by pressing TAB. For example: #add5 

An alternative way to add/delete friends is available in a special interface, 
opens by "Esc" -> "Menu pause" and by clicking on the "MyHome" button.

The controller is supported. 

[Link to mod in Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2561335747)
