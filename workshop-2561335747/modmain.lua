local _G = GLOBAL
local require = _G.require

Assets =
{
    Asset("ANIM", "anim/myhome_markers.zip"),
    Asset("ANIM", "anim/myhome_guestmarker.zip"),
    Asset("ANIM", "anim/myhome_restrictedareas.zip"),
}

PrefabFiles =
{
    "myhome",
    "myhome_builder",
    "myhome_marker",
    "myhome_placemarker",
    "myhome_guestmarker",
    "myhome_informer",
    "myhome_antlion_spawner",
    "myhome_crabking_spawner",
}

_G.TUNING.MYHOME = {}

_G.TUNING.MYHOME.LANGUAGE                  = GetModConfigData("language")
_G.TUNING.MYHOME.ADMIN                     = GetModConfigData("admin")
_G.TUNING.MYHOME.ANNOUNCE                  = GetModConfigData("announce")
_G.TUNING.MYHOME.MIN_WALLS                 = GetModConfigData("min_walls")
_G.TUNING.MYHOME.MAX_WALLS                 = GetModConfigData("max_walls")
_G.TUNING.MYHOME.MIN_DIST_BETWEEN_MYHOMES  = GetModConfigData("min_dist_between_myhomes")
_G.TUNING.MYHOME.INTRUDERS                 = GetModConfigData("intruders")
_G.TUNING.MYHOME.RESTRICTED_AREA           = GetModConfigData("restricted_area")
_G.TUNING.MYHOME.ROBOT_SEPARATE_AREAS_WORK = GetModConfigData("robot_separate_areas_work")
_G.TUNING.MYHOME.PUSHOUT_COMING_BOSSES     = GetModConfigData("pushout_coming_bosses")
_G.TUNING.MYHOME.PUSHOUT_COMING_MOBS       = GetModConfigData("pushout_coming_mobs")
_G.TUNING.MYHOME.PUSHOUT_HAUNTABLE_MOBS    = GetModConfigData("pushout_hauntable_mobs")
_G.TUNING.MYHOME.PUSHOUT_KLAUSSACK         = GetModConfigData("pushout_klaussack")
_G.TUNING.MYHOME.FORCE_PUSHOUT_WOBOT       = GetModConfigData("force_pushout_wobot")
_G.TUNING.MYHOME.ACCESS_RUINS_AREA         = GetModConfigData("access_ruins_area")
_G.TUNING.MYHOME.ACCESS_HERMITCRAB_ISLAND  = GetModConfigData("access_hermitcrab_island")
_G.TUNING.MYHOME.ACCESS_ARCHIVE_MAZE       = GetModConfigData("access_archive_maze")
_G.TUNING.MYHOME.GUEST_GLOMMER_DAYS_ALIVE  = GetModConfigData("guest_glommer_days_alive")
_G.TUNING.MYHOME.GUEST_CHESTER_DAYS_ALIVE  = GetModConfigData("guest_chester_days_alive")
_G.TUNING.MYHOME.GUEST_HUTCH_DAYS_ALIVE    = GetModConfigData("guest_hutch_days_alive")
_G.TUNING.MYHOME.GUEST_WOBOT_DAYS_ALIVE    = GetModConfigData("guest_wobot_days_alive")
_G.TUNING.MYHOME.MIN_WALLS_ADMIN           = 4
_G.TUNING.MYHOME.HOME_HEIGHT               = 5

if _G.TUNING.MYHOME.LANGUAGE                  == nil then _G.TUNING.MYHOME.LANGUAGE                  = "en"  end
if _G.TUNING.MYHOME.ADMIN                     == nil then _G.TUNING.MYHOME.ADMIN                     = true  end
if _G.TUNING.MYHOME.ANNOUNCE                  == nil then _G.TUNING.MYHOME.ANNOUNCE                  = true  end
if _G.TUNING.MYHOME.MIN_WALLS                 == nil then _G.TUNING.MYHOME.MIN_WALLS                 = 16    end
if _G.TUNING.MYHOME.MAX_WALLS                 == nil then _G.TUNING.MYHOME.MAX_WALLS                 = 300   end
if _G.TUNING.MYHOME.MIN_DIST_BETWEEN_MYHOMES  == nil then _G.TUNING.MYHOME.MIN_DIST_BETWEEN_MYHOMES  = 5     end
if _G.TUNING.MYHOME.INTRUDERS                 == nil then _G.TUNING.MYHOME.INTRUDERS                 = true  end
if _G.TUNING.MYHOME.RESTRICTED_AREA           == nil then _G.TUNING.MYHOME.RESTRICTED_AREA           = true  end
if _G.TUNING.MYHOME.ROBOT_SEPARATE_AREAS_WORK == nil then _G.TUNING.MYHOME.ROBOT_SEPARATE_AREAS_WORK = true  end
if _G.TUNING.MYHOME.PUSHOUT_COMING_BOSSES     == nil then _G.TUNING.MYHOME.PUSHOUT_COMING_BOSSES     = true  end
if _G.TUNING.MYHOME.PUSHOUT_COMING_MOBS       == nil then _G.TUNING.MYHOME.PUSHOUT_COMING_MOBS       = true  end
if _G.TUNING.MYHOME.PUSHOUT_HAUNTABLE_MOBS    == nil then _G.TUNING.MYHOME.PUSHOUT_HAUNTABLE_MOBS    = true  end
if _G.TUNING.MYHOME.PUSHOUT_KLAUSSACK         == nil then _G.TUNING.MYHOME.PUSHOUT_KLAUSSACK         = true  end
if _G.TUNING.MYHOME.FORCE_PUSHOUT_WOBOT       == nil then _G.TUNING.MYHOME.FORCE_PUSHOUT_WOBOT       = false end
if _G.TUNING.MYHOME.ACCESS_RUINS_AREA         == nil then _G.TUNING.MYHOME.ACCESS_RUINS_AREA         = false end
if _G.TUNING.MYHOME.ACCESS_HERMITCRAB_ISLAND  == nil then _G.TUNING.MYHOME.ACCESS_HERMITCRAB_ISLAND  = false end
if _G.TUNING.MYHOME.ACCESS_ARCHIVE_MAZE       == nil then _G.TUNING.MYHOME.ACCESS_ARCHIVE_MAZE       = false end
if _G.TUNING.MYHOME.GUEST_GLOMMER_DAYS_ALIVE  == nil then _G.TUNING.MYHOME.GUEST_GLOMMER_DAYS_ALIVE  = 0     end
if _G.TUNING.MYHOME.GUEST_CHESTER_DAYS_ALIVE  == nil then _G.TUNING.MYHOME.GUEST_CHESTER_DAYS_ALIVE  = 0     end
if _G.TUNING.MYHOME.GUEST_HUTCH_DAYS_ALIVE    == nil then _G.TUNING.MYHOME.GUEST_HUTCH_DAYS_ALIVE    = 0     end
if _G.TUNING.MYHOME.GUEST_WOBOT_DAYS_ALIVE    == nil then _G.TUNING.MYHOME.GUEST_WOBOT_DAYS_ALIVE    = 0     end

_G.STRINGS.NAMES.MYHOME_ANTLION_SPAWNER = _G.STRINGS.NAMES.ANTLION
_G.STRINGS.NAMES.MYHOME_CRABKING_SPAWNER = _G.STRINGS.NAMES.CRABKING

modimport("scripts/languages/mh_".._G.TUNING.MYHOME.LANGUAGE..".lua")
modimport("scripts/myhome_actions.lua")
modimport("scripts/myhome_commands.lua")

local StorageRobotCommon = require("prefabs/storage_robot_common")
local prefabdefs = require("myhome_prefabdefs")

for _, v in ipairs(prefabdefs.wall_prefabs) do
    AddPrefabPostInit(v, function(inst)
        if not inst.components.myhome_wall then
            inst:AddComponent("myhome_wall")
        end
    end)
end

for _, v in ipairs({"forest_network", "cave_network"}) do
    AddPrefabPostInit(v, function(inst)
        inst:AddComponent("myhome_manager")
    end)
end

--help markers (only client side)
if (_G.TheNet:GetIsClient() or _G.TheNet:GetServerIsClientHosted()) then
    if _G.TUNING.MYHOME.RESTRICTED_AREA then
        local isadmin = false
        local client = _G.TheNet:GetClientTableForUser(_G.TheNet:GetUserID())
        if _G.TUNING.MYHOME.ADMIN and client and client.admin then
            isadmin = true
        end
        if not isadmin then
            modimport("scripts/myhome_restrictedarea_helper.lua")
        end
    end

    for _, v in ipairs(prefabdefs.wall_prefabs) do
        AddPrefabPostInit(v.."_item_placer", function(inst)
            inst.deployhelper_key = "myhomewall"
            inst.mh_placemarker = _G.SpawnPrefab("myhome_placemarker")
            inst.mh_placemarker.entity:SetParent(inst.entity)
        end)
    end
end

--patch scoreboard, show numbers if server is client hosted
if _G.TheNet:GetServerIsClientHosted() then
    AddClassPostConstruct("screens/playerstatusscreen", function(self)
        local original_DoInit = self.DoInit

        self.DoInit = function(self, ClientObjs)
            original_DoInit(self, ClientObjs)
            if not self.scroll_list.myhome_old_listupdatefn then
                self.scroll_list.myhome_old_listupdatefn = self.scroll_list.updatefn
                self.scroll_list.updatefn = function(playerListing, client, i)
                    self.scroll_list.myhome_old_listupdatefn(playerListing, client, i)
                    if not playerListing.shown then return end
                    playerListing.number:Show()
                end
                self.scroll_list:RefreshView()
            end
        end
    end)
end

local MyHomePopupScreen = require("screens/myhomepopupscreen")
AddClassPostConstruct("screens/redux/pausescreen", function(self)
    local last = self.menu:AddItem(_G.STRINGS.MYHOME.MENU_BUTTON_MYHOME, function()
        self:unpause()
        _G.TheFrontEnd:PushScreen(MyHomePopupScreen(self.owner))
    end)

    local button_h = 42
    local numbuttons = #self.menu.items
    local menupos_y = ((button_h * (numbuttons + 1)) / 2)

    for i = numbuttons, 2, -1 do
        self.menu.items[i] = self.menu.items[i - 1]
    end

    self.menu.items[1] = last
    self.menu.offset = -button_h
    self.menu.pos = _G.Vector3(0, 0, 0)

    for i, v in ipairs(self.menu.items) do
        self.menu.pos.y = self.menu.pos.y + self.menu.offset
        v:SetPosition(self.menu.pos)
        v:SetScale(.65)
    end

    if self.pause_button_index ~= nil then
        self.pause_button_index = self.pause_button_index + 1
    end

    if self.options_button_index ~= nil then
        self.options_button_index = self.options_button_index + 1
    end

    self.menu:SetPosition(0, menupos_y, 0)
    self.menu:DoFocusHookups()
    self:UpdateText()
end)

modimport("scripts/myhome_mods_compatibility.lua")

-- Server only ------------------------------------------------------------
if not _G.TheNet:GetIsClient() then

    modimport("scripts/myhome_fns.lua")

    if _G.TUNING.MYHOME.RESTRICTED_AREA then
        for i, tbl in ipairs(prefabdefs.restricted_prefabs) do
            for _, v in ipairs(tbl) do
                AddPrefabPostInit(v, function(inst)
                    inst:AddTag("mh_r"..i)
                end)
            end
        end
        --Note: "myhome_antlion_spawner" spawn only once in world
        AddPrefabPostInit("antlion_spawner", function(inst)
            inst:DoTaskInTime(0, function(ent)
                local x, y, z = ent.Transform:GetWorldPosition()
                local ents = _G.TheSim:FindEntities(x, y, z, 0.5, {"antlion_spawner_restricted"})
                if ents[1] == nil then
                    local spawned = _G.SpawnPrefab("myhome_antlion_spawner")
                    if spawned ~= nil then
                        spawned.Transform:SetPosition(x, y, z)
                    end
                end
            end)
        end)
        --Note: "myhome_crabking_spawner" spawn only once in world
        AddPrefabPostInit("crabking_spawner", function(inst)
            inst:DoTaskInTime(0, function(ent)
                local x, y, z = ent.Transform:GetWorldPosition()
                local ents = _G.TheSim:FindEntities(x, y, z, 0.5, {"crabking_spawner_restricted"})
                if ents[1] == nil then
                    local spawned = _G.SpawnPrefab("myhome_crabking_spawner")
                    if spawned ~= nil then
                        spawned.Transform:SetPosition(x, y, z)
                    end
                end
            end)
        end)
    end

    if _G.TUNING.MYHOME.PUSHOUT_COMING_BOSSES then
        for _, v in ipairs(prefabdefs.coming_bosses_prefabs) do
            AddPrefabPostInit(v, function(inst)
                inst:AddComponent("myhome_trackable")
                inst.components.myhome_trackable.testforce = true
                inst.components.myhome_trackable:SetOnAfterTestForceFn(function(ent)
                    ent.components.myhome_trackable:Pause()
                end)
                inst.components.myhome_trackable:SetOnTeleportedInsideFn(function(ent, home)
                    ent.components.myhome_trackable:Pause()
                end)
                inst.components.myhome_trackable:SetOnSuccessPushFn(function(ent, oldpos, newpos)
                    ent.components.myhome_trackable:Pause()
                end)
                inst.components.myhome_trackable:SetOnFailPushFn(function(ent, fromhome, pos)
                    ent:Remove()
                end)
            end)
        end
    end

    if _G.TUNING.MYHOME.PUSHOUT_COMING_MOBS then
        for _, v in ipairs(prefabdefs.coming_mobs_prefabs) do
            AddPrefabPostInit(v, function(inst)
                inst:AddComponent("myhome_trackable")
                inst.components.myhome_trackable.testforce = true
                inst.components.myhome_trackable:SetOnAfterTestForceFn(function(ent)
                    ent.components.myhome_trackable:Pause()
                end)
                inst.components.myhome_trackable:SetOnTeleportedInsideFn(function(ent, home)
                    ent.components.myhome_trackable:Pause()
                end)
                inst.components.myhome_trackable:SetOnSuccessPushFn(function(ent, oldpos, newpos)
                    ent.components.myhome_trackable:Pause()
                end)
                inst.components.myhome_trackable:SetOnFailPushFn(function(ent, fromhome, pos)
                    ent:Remove()
                end)
            end)
        end
    end

    if _G.TUNING.MYHOME.PUSHOUT_HAUNTABLE_MOBS then
        for _, v in ipairs(prefabdefs.hauntable_mobs_prefabs) do
            AddPrefabPostInit(v, function(inst)
                inst:AddComponent("myhome_trackable")
                inst.components.myhome_trackable.testforce = true
                inst.components.myhome_trackable:SetOnAfterTestForceFn(function(ent)
                    ent.components.myhome_trackable:Pause()
                end)
                inst.components.myhome_trackable:SetOnTeleportedInsideFn(function(ent, home)
                    ent.components.myhome_trackable:Pause()
                end)
                inst.components.myhome_trackable:SetOnSuccessPushFn(function(ent, oldpos, newpos)
                    ent.components.myhome_trackable:Pause()
                end)
                inst.components.myhome_trackable:SetOnFailPushFn(function(ent, fromhome, pos)
                    ent:Remove()
                end)
            end)
        end
    end

    if _G.TUNING.MYHOME.PUSHOUT_KLAUSSACK then
        for _, v in ipairs(prefabdefs.klaussack_prefabs) do
            AddPrefabPostInit(v, function(inst)
                inst:AddComponent("myhome_trackable")
            end)
        end
    end

    if _G.TUNING.MYHOME.GUEST_GLOMMER_DAYS_ALIVE ~= nil and
       _G.TUNING.MYHOME.GUEST_GLOMMER_DAYS_ALIVE > 0 then
        AddPrefabPostInit("glommer", function(inst)
            inst:AddComponent("myhome_guestobserver")
            inst.components.myhome_guestobserver:SetLimit(_G.TUNING.TOTAL_DAY_TIME * _G.TUNING.MYHOME.GUEST_GLOMMER_DAYS_ALIVE)
            if _G.TUNING.MYHOME.ANNOUNCE then
                inst.components.myhome_guestobserver:SetOnMurderFn(function(ent)
                    local shardid = tostring(_G.TheShard:GetShardId() or 0) --Note: GetShardId function return string type
                    if shardid ~= "0" then
                        _G.TheNet:Announce(string.format(_G.STRINGS.MYHOME.ANNOUNCE_GLOMMER_MURDER_IN_SHARD_WORLD, shardid), nil, nil, "death")
                    else
                        _G.TheNet:Announce(_G.STRINGS.MYHOME.ANNOUNCE_GLOMMER_MURDER_IN_WORLD, nil, nil, "death")
                    end
                end)
            end
        end)
    end

    if _G.TUNING.MYHOME.GUEST_CHESTER_DAYS_ALIVE ~= nil and
       _G.TUNING.MYHOME.GUEST_CHESTER_DAYS_ALIVE > 0 then
        AddPrefabPostInit("chester", function(inst)
            inst:AddComponent("myhome_guestobserver")
            inst.components.myhome_guestobserver:SetLimit(_G.TUNING.TOTAL_DAY_TIME * _G.TUNING.MYHOME.GUEST_CHESTER_DAYS_ALIVE)
            if _G.TUNING.MYHOME.ANNOUNCE then
                inst.components.myhome_guestobserver:SetOnMurderFn(function(ent)
                    local shardid = tostring(_G.TheShard:GetShardId() or 0) --Note: GetShardId function return string type
                    if shardid ~= "0" then
                        _G.TheNet:Announce(string.format(_G.STRINGS.MYHOME.ANNOUNCE_CHESTER_MURDER_IN_SHARD_WORLD, shardid), nil, nil, "death")
                    else
                        _G.TheNet:Announce(_G.STRINGS.MYHOME.ANNOUNCE_CHESTER_MURDER_IN_WORLD, nil, nil, "death")
                    end
                end)
            end
        end)
    end

    if _G.TUNING.MYHOME.GUEST_HUTCH_DAYS_ALIVE ~= nil and
       _G.TUNING.MYHOME.GUEST_HUTCH_DAYS_ALIVE > 0 then
        AddPrefabPostInit("hutch", function(inst)
            inst:AddComponent("myhome_guestobserver")
            inst.components.myhome_guestobserver:SetLimit(_G.TUNING.TOTAL_DAY_TIME * _G.TUNING.MYHOME.GUEST_HUTCH_DAYS_ALIVE)
            if _G.TUNING.MYHOME.ANNOUNCE then
                inst.components.myhome_guestobserver:SetOnMurderFn(function(ent)
                    local shardid = tostring(_G.TheShard:GetShardId() or 0) --Note: GetShardId function return string type
                    if shardid ~= "0" then
                        _G.TheNet:Announce(string.format(_G.STRINGS.MYHOME.ANNOUNCE_HUTCH_MURDER_IN_SHARD_WORLD, shardid), nil, nil, "death")
                    else
                        _G.TheNet:Announce(_G.STRINGS.MYHOME.ANNOUNCE_HUTCH_MURDER_IN_WORLD, nil, nil, "death")
                    end
                end)
            end
        end)
    end

    if _G.TUNING.MYHOME.FORCE_PUSHOUT_WOBOT then
        AddPrefabPostInit("storage_robot", function(inst)
            inst:AddComponent("myhome_trackable")
            inst.components.myhome_trackable.optfx.old = "spawn_fx_medium"
            inst.components.myhome_trackable.optfx.new = "spawn_fx_medium"
            inst.components.myhome_trackable.testforce = true
            inst.components.myhome_trackable:SetOnSuccessPushFn(function(ent, oldpos, newpos)
                StorageRobotCommon.UpdateSpawnPoint(ent)
                if ent._sleeptask ~= nil and not ent:IsAsleep() then
                    ent._sleeptask:Cancel()
                    ent._sleeptask = nil
                end
            end)
        end)
    else
        AddPrefabPostInit("junk_pile_big", function(inst)
            --Note: Register JunkPile when position sets
            inst:DoTaskInTime(0, function(ent)
                _G.TheWorld.net.components.myhome_manager:RegisterJunkPileInWorld(ent)
            end)
        end)

        if _G.TUNING.MYHOME.GUEST_WOBOT_DAYS_ALIVE ~= nil and
           _G.TUNING.MYHOME.GUEST_WOBOT_DAYS_ALIVE > 0 then
            AddPrefabPostInit("storage_robot", function(inst)
                inst:AddComponent("myhome_guestobserver")
                inst.components.myhome_guestobserver:SetTypeWOBOT()
                inst.components.myhome_guestobserver:SetLimit(_G.TUNING.TOTAL_DAY_TIME * _G.TUNING.MYHOME.GUEST_WOBOT_DAYS_ALIVE)
                if _G.TUNING.MYHOME.ANNOUNCE then
                    inst.components.myhome_guestobserver:SetOnMoveToJunkPileFn(function(ent)
                        local shardid = tostring(_G.TheShard:GetShardId() or 0) --Note: GetShardId function return string type
                        if shardid ~= "0" then
                            _G.TheNet:Announce(string.format(_G.STRINGS.MYHOME.ANNOUNCE_WOBOT_MOVE_TO_JUNK_PILE_IN_SHARD_WORLD, shardid), nil, nil, "death")
                        else
                            _G.TheNet:Announce(_G.STRINGS.MYHOME.ANNOUNCE_WOBOT_MOVE_TO_JUNK_PILE_IN_WORLD, nil, nil, "death")
                        end
                    end)
                    inst.components.myhome_guestobserver:SetOnKickOutFn(function(ent)
                        local shardid = tostring(_G.TheShard:GetShardId() or 0) --Note: GetShardId function return string type
                        if shardid ~= "0" then
                            _G.TheNet:Announce(string.format(_G.STRINGS.MYHOME.ANNOUNCE_WOBOT_KICK_OUT_IN_SHARD_WORLD, shardid), nil, nil, "death")
                        else
                            _G.TheNet:Announce(_G.STRINGS.MYHOME.ANNOUNCE_WOBOT_KICK_OUT_IN_WORLD, nil, nil, "death")
                        end
                    end)
                end
            end)
        end
    end

    AddPrefabPostInit("tumbleweed", function(inst)
        inst:DoTaskInTime(0, function(ent)
            local x, y, z = ent.Transform:GetWorldPosition()
            local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z, true, 4)
            if home ~= nil then
                local newpos = home:GetPointOutside(ent:GetPhysicsRadius(0), 8)
                if newpos ~= nil then
                    ent.Transform:SetPosition(newpos.x, newpos.y, newpos.z)
                end
            end
        end)
    end)

    for _, prefab in ipairs(prefabdefs.tracked_prefabs) do
        AddPrefabPostInit(prefab, function(inst)
            if inst.prefab == "lureplant" then
                inst:DoTaskInTime(0, function(inst)
                    if not inst:HasTag("planted") then
                        inst:AddComponent("myhome_trackable")
                        inst.components.myhome_trackable.testforce = true
                        inst.components.myhome_trackable:SetOnFailPushFn(function(ent, fromhome, pos)
                            ent:Remove()
                        end)
                    end
                end)
            else
                inst:AddComponent("myhome_trackable")
            end

            if inst.prefab == "moon_altar_astral_marker_1" or
               inst.prefab == "moon_altar_astral_marker_2" or
               inst.prefab == "lost_toy_1" or
               inst.prefab == "lost_toy_2" or
               inst.prefab == "lost_toy_7" or
               inst.prefab == "lost_toy_10" or
               inst.prefab == "lost_toy_11" or
               inst.prefab == "lost_toy_14" or
               inst.prefab == "lost_toy_18" or
               inst.prefab == "lost_toy_19" or
               inst.prefab == "lost_toy_42" or
               inst.prefab == "lost_toy_43" then
                inst.components.myhome_trackable.optfx.old = nil
                inst.components.myhome_trackable.optfx.new = nil
            end

            if inst.prefab == "dirtpile" then
                inst.components.myhome_trackable:SetOnSuccessPushFn(function(ent, oldpos, newpos)
                    --check previos animal_track look at dirtpile and set new rotation
                    local ents = _G.TheSim:FindEntities(oldpos.x, oldpos.y, oldpos.z, _G.TUNING.MAX_DIRT_DISTANCE, {"track"})
                    for _, v in pairs(ents) do
                        if v.prefab == "animal_track" then
                            local pos = v:GetPosition()
                            local rotate = -(v.Transform:GetRotation() + 90)
                            local dx = _G.TUNING.HUNT_SPAWN_DIST * math.cos(rotate * _G.DEGREES)
                            local dz = _G.TUNING.HUNT_SPAWN_DIST * math.sin(rotate * _G.DEGREES)
                            local pos_taget = pos + _G.Vector3(dx, 0, dz)
                            local dist = _G.VecUtil_Dist(oldpos.x, oldpos.z, pos_taget.x, pos_taget.z)
                            if dist < 0.5 then
                                local angle = v:GetAngleToPoint(newpos.x, newpos.y, newpos.z) - 90
                                v.Transform:SetRotation(angle)
                                break
                            end
                        end
                    end
                end)
            end
        end)
    end

    local function TestPushPlayer(inst)
        local x, y, z = inst.Transform:GetWorldPosition()
        local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)
        if inst:HasTag("player") and not inst:HasTag("playerghost") and
           home ~= nil and not home:IsAvailableForDoer(inst) then
            home:DoPushForce(inst)
        end
    end

    AddStategraphPostInit("wilson", function(self)
        self.states["washed_ashore"].onexit = function(inst)
            TestPushPlayer(inst)
        end

        local pocketwatch_warpback_pst_onexit = self.states["pocketwatch_warpback_pst"].onexit
        self.states["pocketwatch_warpback_pst"].onexit = function(inst)
            pocketwatch_warpback_pst_onexit(inst)
            TestPushPlayer(inst)
        end

        local pocketwatch_portal_land_onexit = self.states["pocketwatch_portal_land"].onexit
        self.states["pocketwatch_portal_land"].onexit = function(inst)
            pocketwatch_portal_land_onexit(inst)
            TestPushPlayer(inst)
        end
    end)

    AddStategraphPostInit("boatcannon", function(self)
        local shoot_onenter = self.states["shoot"].onenter
        self.states["shoot"].onenter = function(inst, data)
            shoot_onenter(inst, data)
            inst.sg.statemem.mh_owner = inst.components.boatcannon.operator 
        end

        self.states["shoot"].timeline[1].fn = function(inst)
            inst.components.boatcannon:Shoot(inst.sg.statemem.mh_owner)
        end
    end)

    --Note: force push after spawn for players sets in myhome_manager components
    if _G.TUNING.MYHOME.INTRUDERS then
        AddPlayerPostInit(function(inst)
            inst:AddComponent("myhome_trackable")
            inst.components.myhome_trackable:SetOnTeleportedInsideFn(function(ent, home)
                if ent:HasTag("player") and not ent:HasTag("playerghost") and
                   not home:IsAvailableForDoer(ent) then
                    home:DoPushForce(ent)
                end
            end)
        end)
    end

    --Note: original shadowwaxwellbrain.lua last updated 08.03.2024
    local shadowwaxwellbrain = require("myhome_shadowwaxwellbrain_override")
    AddPrefabPostInit("shadowworker", function(inst)
        inst:SetBrain(shadowwaxwellbrain)
    end)

    --Note: original pollyrogerbrain.lua last updated 30.11.2022
    local pollyrogerbrain = require("myhome_pollyrogerbrain_override")
    AddPrefabPostInit("polly_rogers", function(inst)
        inst:SetBrain(pollyrogerbrain)
    end)

    AddPrefabPostInit("wagstaff_npc", function(inst)
        inst:DoTaskInTime(0, function(ent)
            local x, y, z = ent.Transform:GetWorldPosition()
            local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z, true, 10)
            if home ~= nil then
                local newpos = home:GetPointOutside(ent:GetPhysicsRadius(0), 10)
                if newpos ~= nil then
                    ent.Transform:SetPosition(newpos.x, newpos.y, newpos.z)
                end
            end
        end)

        inst:ListenForEvent("teleported", function(ent)
            local x, y, z = ent.Transform:GetWorldPosition()
            local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z, true, 10)
            if home ~= nil then
                local newpos = home:GetPointOutside(ent:GetPhysicsRadius(0), 10)
                if newpos ~= nil then
                    ent.Transform:SetPosition(newpos.x, newpos.y, newpos.z)
                end
            end

            if ent.static then
                local pos = ent:GetPosition()
                local radius = 1
                local theta = (ent.Transform:GetRotation() + 90) * _G.DEGREES
                local offset = _G.Vector3(radius * math.cos(theta), 0, -radius * math.sin(theta))
                ent.static.Transform:SetPosition(pos.x + offset.x, pos.y, pos.z + offset.z)
                ent:DoTaskInTime(0, function() 
                    ent:ForceFacePoint(pos.x, pos.y, pos.z)
                end)
            end
        end)
    end)

    AddComponentPostInit("moonstormmanager", function(self)
        local original_FindUnmetCharacter = self.FindUnmetCharacter
        local original_GetNewWagstaffLocation = self.GetNewWagstaffLocation

        self.FindUnmetCharacter = function(self)
            local pos = original_FindUnmetCharacter(self)
            if pos ~= nil then
                local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(pos.x, pos.y, pos.z, true, 10)
                if home ~= nil then
                    pos = nil
                end
            end
            return pos
        end

        self.GetNewWagstaffLocation = function(self, wagstaff)
            local pos = original_GetNewWagstaffLocation(self, wagstaff)
            if pos ~= nil then
                local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(pos.x, pos.y, pos.z, true, 10)
                if home ~= nil then
                    local newpos = home:GetPointOutside(wagstaff:GetPhysicsRadius(0), 10)
                    if newpos ~= nil then
                        pos = newpos
                    end
                end
            end
            return pos
        end
    end)

    AddComponentPostInit("burnable", function(self)
        local original_StartWildfire = self.StartWildfire

        self.StartWildfire = function(self)
            local founded_myhome = false
            local x, y, z = self.inst.Transform:GetWorldPosition()
            local ents = _G.TheSim:FindEntities(x, 0, z, 8, {"mh_lock"})
            if ents[1] ~= nil then --pass 1, test outside (or inside, if wall near)
                founded_myhome = true
            elseif _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z) ~= nil then --pass 2, test inside
                founded_myhome = true
            end
            if not founded_myhome then
                original_StartWildfire(self)
            end
        end
    end)

    AddComponentPostInit("propagator", function(self)
        local original_StartUpdating = self.StartUpdating

        self.StartUpdating = function(self)
            original_StartUpdating(self)
            if not self.damages then
                local founded_myhome = false
                local x, y, z = self.inst.Transform:GetWorldPosition()
                local ents = _G.TheSim:FindEntities(x, 0, z, 8, {"mh_lock"})
                if ents[1] ~= nil then --pass 1, test outside (or inside, if wall near)
                    founded_myhome = true
                elseif _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z) ~= nil then --pass 2, test inside
                    founded_myhome = true
                end
                if founded_myhome then
                    self:StopUpdating()
                    self:StopSpreading(true)
                end
            end
        end
    end)

    AddComponentPostInit("weapon", function(self)
        local original_OnAttack = self.OnAttack

        self.OnAttack = function(self, attacker, target, projectile)
            if target ~= nil and target:IsValid() and
               projectile ~= nil and type(projectile) == "table" and projectile.prefab == "fire_projectile" then
                local canattack = true
                local x, y, z = target.Transform:GetWorldPosition()
                local ents = _G.TheSim:FindEntities(x, 0, z, 8, {"mh_lock"})
                --pass 1
                for _, wall in ipairs(ents) do
                    local homeindex = wall.components.myhome_wall.homeindex
                    if homeindex > 0 then
                        local home = _G.TheWorld.net.components.myhome_manager:GetHomeByHomeIndex(homeindex)
                        if home ~= nil and not home:IsAvailableForDoer(attacker) then
                            canattack = false
                            break
                        end
                    end
                end
                --pass 2
                if canattack then
                    local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)
                    if home ~= nil and not home:IsAvailableForDoer(attacker) then
                        canattack = false
                    end
                end
                --pass 3
                if not canattack then
                    if attacker:HasTag("player") and attacker.components.talker ~= nil then
                        attacker.components.talker:Say(_G.STRINGS.MYHOME.THIS_IS_HOME_ANOTHER, 3)
                    end
                    return false
                end
            end

            original_OnAttack(self, attacker, target, projectile)
        end
    end)

    AddComponentPostInit("autoterraformer", function(self)
        local original_DoTerraform = self.DoTerraform

        self.DoTerraform = function(self, px, py, pz, x, y)
            --pass1, test inside        
            local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(px, py, pz) 
            --pass2, test outside, range 8 (or inside, if wall near)
            if not home then
                local walls = _G.TheSim:FindEntities(px, py, pz, 8, {"mh_lock"})
                for _, wall in ipairs(walls) do
                    local homeindex = wall.components.myhome_wall.homeindex
                    if homeindex > 0 then
                        home = _G.TheWorld.net.components.myhome_manager:GetHomeByHomeIndex(homeindex)
                        if home then
                            break
                        end
                    end
                end
            end

            local owner = nil

            local ents = _G.TheSim:FindEntities(px, py, pz, 0.1, {"player"})
            for _, ent in ipairs(ents) do
                local equipped_head = ent.components.inventory ~= nil and ent.components.inventory:GetEquippedItem(_G.EQUIPSLOTS.HEAD) or nil
                if self.inst == equipped_head then
                    owner = ent
                    break
                end
            end

            if home then
                if owner then
                    if home:IsAvailableForDoer(owner) then
                        return original_DoTerraform(self, px, py, pz, x, y)
                    else
                        --not friend
                        return
                    end
                else
                    --pigs, spiders e.t.c
                    return
                end
            end

            return original_DoTerraform(self, px, py, pz, x, y)
        end
    end)

    AddComponentPostInit("inspectaclesparticipants", function(self)
        local original_CreateBox = self.CreateBox
        self.CreateBox = function(self)
            original_CreateBox(self)
            if self.box ~= nil then
                self.box:DoTaskInTime(0, function(ent)
                    inst:AddComponent("myhome_trackable")
                    inst.components.myhome_trackable.testforce = true
                end)
            end
        end
    end)

    AddClassPostConstruct("components/deployable", function(self)
        local original_Deploy = self.Deploy

        self.Deploy = function(self, pt, deployer, rot)
            local res = original_Deploy(self, pt, deployer, rot)
            if res and deployer then
                local x = math.floor(pt.x) + .5
                local z = math.floor(pt.z) + .5
                local ents = _G.TheSim:FindEntities(x, 0, z, 0.2, {"wall"})
                for _, ent in ipairs(ents) do
                    if ent.components.myhome_wall then
                        _G.TheWorld.net.components.myhome_manager:OnDeployWall(ent, deployer)
                    end
                end
            end
            return res
        end
    end)

    modimport("scripts/myhome_actions_override.lua")
    modimport("scripts/myhome_boatcannon_override.lua")       --Note: original component boatcannon.lua last updated 17.08.2022
    modimport("scripts/myhome_cannonballs_override.lua")      --Note: original prefab cannonballs.lua last updated 28.06.2024
    modimport("scripts/myhome_orangeamulet_override.lua")     --Note: original prefab amulet.lua last updated 01.10.2023
    modimport("scripts/myhome_abigail_override.lua")          --Note: original prefab abigail.lua last updated 01.10.2023
    modimport("scripts/myhome_shadowwaxwell_override.lua")    --Note: original prefab shadowwaxwell.lua last updated 06.06.2024
    modimport("scripts/myhome_walkableplatform_override.lua") --Note: original component walkableplatform.lua last updated 03.07.2024

    --Save legacy commands 
    local cmd_choose = {"add", "del"}
    local original_Networking_Say = _G.Networking_Say
    _G.Networking_Say = function(guid, userid, name, prefab, message, colour, whisper, isemote, ...)
        original_Networking_Say(guid, userid, name, prefab, message, colour, whisper, isemote, ...)
        local msg = string.lower(message)
        local hashtag = string.find(msg, "#")
        if hashtag then
            local scoreindex = nil
            local mode = nil
            for _, v in ipairs(cmd_choose) do
                scoreindex = string.match(msg, v.."%s*(%d+)", hashtag + 1)

                if scoreindex then
                    scoreindex = _G.tonumber(scoreindex)
                    mode = v
                    break
                end
            end

            if mode == nil then return end

            local myhome_manager = _G.TheWorld.net.components.myhome_manager
            local isdedicated = not _G.TheNet:GetServerIsClientHosted()
            local index = 1
            local founduserid = nil
            local doer = _G.Ents[guid]
            for _, v in ipairs(_G.TheNet:GetClientTable()) do
                if not isdedicated or v.performance == nil then
                    if index == scoreindex then
                        founduserid = v.userid
                        break
                    end
                    index = index + 1
                end
            end

            if founduserid ~= nil and doer ~= nil then
                myhome_manager:CommandFriends(mode, doer, founduserid)
            end
        end
    end
end -- end Server only
