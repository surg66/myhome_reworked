local _G = GLOBAL

local COMBAT_MUSHAVE_TAGS = { "_combat", "_health" }
local COMBAT_CANTHAVE_TAGS = { "INLIMBO", "companion" }
local COMBAT_MUSTONEOF_TAGS_AGGRESSIVE = { "monster", "prey", "insect", "hostile", "character", "animal" }

local function HasFriendlyLeader(inst, target)
    local leader = inst.components.follower.leader
    if leader ~= nil then
        local target_leader = (target.components.follower ~= nil) and target.components.follower.leader or nil

        if target_leader and target_leader.components.inventoryitem then
            target_leader = target_leader.components.inventoryitem:GetGrandOwner()
            -- Don't attack followers if their follow object has no owner
            if target_leader == nil then
                return true
            end
        end

        local PVP_enabled = _G.TheNet:GetPVPEnabled()

        return leader == target or (target_leader ~= nil
                and (target_leader == leader or (target_leader:HasTag("player")
                and not PVP_enabled))) or
                (target.components.domesticatable and target.components.domesticatable:IsDomesticated()
                and not PVP_enabled) or
                (target.components.saltlicker and target.components.saltlicker.salted
                and not PVP_enabled)
    end

    return false
end

local function protectorretargetfn(inst)
    if inst.sg:HasStateTag("dancing") then
        return nil
    end

    local spawn = inst:GetSpawnPoint()
    if spawn == nil then
        return nil
    end

    local target = nil
    local ents = _G.TheSim:FindEntities(spawn.x, spawn.y, spawn.z, _G.TUNING.SHADOWWAXWELL_PROTECTOR_DEFEND_RADIUS, COMBAT_MUSHAVE_TAGS, COMBAT_CANTHAVE_TAGS, COMBAT_MUSTONEOF_TAGS_AGGRESSIVE)

    for _, ent in ipairs(ents) do
        if ent ~= inst and ent.entity:IsVisible()
        and inst.components.combat:CanTarget(ent)
        and ent.components.minigame_participator == nil
        and not HasFriendlyLeader(inst, ent) then
            -- logic for test MyHome
            local available = true
            local leader = inst.components.follower.leader
            if leader ~= nil then
                local x, y, z = ent.Transform:GetWorldPosition()
                local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)
                if home ~= nil and not home:IsAvailableForDoer(leader) then
                    available = false
                end
            end
            if available then
                target = ent
                break
            end
        end
    end

    return target
end

local function protectorkeeptargetfn(inst, target)
    -- Maintain the target if it is able to.
    local res = inst.components.combat:CanTarget(target)
        and not inst.sg:HasStateTag("dancing")
        and target.components.minigame_participator == nil
        and (not target:HasTag("player") or _G.TheNet:GetPVPEnabled())

    if res then
        local leader = inst.components.follower.leader
        if leader ~= nil then
            local x, y, z = target.Transform:GetWorldPosition()
            local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)
            if home ~= nil and not home:IsAvailableForDoer(leader) then
                return false
            end
        end
    end

    return res
end

AddPrefabPostInit("shadowprotector", function(inst)
    if not _G.TheWorld.ismastersim then
        return inst
    end

    inst.components.combat:SetRetargetFunction(1, protectorretargetfn)
    inst.components.combat:SetKeepTargetFunction(protectorkeeptargetfn)
end)
