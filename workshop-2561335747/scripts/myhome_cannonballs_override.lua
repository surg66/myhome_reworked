local _G = GLOBAL

local function OnHit(inst, attacker, target)
    local canhit = true
    local x, y, z = inst.Transform:GetWorldPosition()
    local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)

    if home ~= nil then
        canhit = false

        if inst.mh_owner and inst.mh_owner:IsValid() and home:IsAvailableForDoer(inst.mh_owner) then
            canhit = true
        end        
    end

    if canhit then
        inst.components.complexprojectile.myhome_onhitfn(inst, attacker, target)
    else
        if inst:IsOnOcean() then
            _G.SpawnPrefab("crab_king_waterspout").Transform:SetPosition(x, y, z)
        else
            _G.SpawnPrefab("cannonball_used").Transform:SetPosition(x, y, z)

            if _G.TheWorld.components.dockmanager ~= nil then
                -- Damage any docks we hit.
                _G.TheWorld.components.dockmanager:DamageDockAtPoint(x, y, z, _G.TUNING.CANNONBALL_DAMAGE)
            end
        end
        inst:Remove()
    end
end

local function OnUpdateProjectile(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)

    local update = true
    if home ~= nil then
        update = false
        if inst.mh_owner and inst.mh_owner:IsValid() and home:IsAvailableForDoer(inst.mh_owner) then
            update = true
        end
    end

    if update then
        inst.components.complexprojectile.myhome_onupdatefn(inst)
    end
end

AddPrefabPostInit("cannonball_rock", function(inst)
    if not _G.TheWorld.ismastersim then
        return inst
    end

    inst.components.complexprojectile.myhome_onhitfn = inst.components.complexprojectile.onhitfn
    inst.components.complexprojectile.myhome_onupdatefn = inst.components.complexprojectile.onupdatefn
    inst.components.complexprojectile:SetOnHit(OnHit)
    inst.components.complexprojectile:SetOnUpdate(OnUpdateProjectile)
end)
