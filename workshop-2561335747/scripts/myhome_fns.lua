local _G = GLOBAL

local PICKUP_MUST_ONEOF_TAGS = { "_inventoryitem", "pickable" }
local PICKUP_CANT_TAGS = {
    -- Items
    "INLIMBO", "NOCLICK", "irreplaceable", "knockbackdelayinteraction", "event_trigger",
    "minesprung", "mineactive", "catchable",
    "fire", "light", "spider", "cursed", "paired", "bundle",
    "heatrock", "deploykititem", "boatbuilder", "singingshell",
    "archive_lockbox", "simplebook", "furnituredecor",
    -- Pickables
    "flower", "gemsocket", "structure",
    -- Either
    "donotautopick",
}

local function FindPickupableItem_filter(v, ba, owner, radius, furthestfirst, positionoverride, ignorethese, onlytheseprefabs, allowpickables, ispickable, worker, extra_filter)
    if extra_filter ~= nil and not extra_filter(worker, v, owner) then
        return false
    end
    if _G.AllBuilderTaggedRecipes[v.prefab] then
        return false
    end
    -- NOTES(JBK): "donotautopick" for general class components here.
    if v.components.armor or v.components.weapon or v.components.tool or v.components.equippable or v.components.sewing or v.components.erasablepaper then
        return false
    end
    if v.components.burnable ~= nil and (v.components.burnable:IsBurning() or v.components.burnable:IsSmoldering()) then
        return false
    end
    if ispickable then
        if not allowpickables then
            return false
        end
    else
        if not (v.components.inventoryitem ~= nil and
            v.components.inventoryitem.canbepickedup and
            v.components.inventoryitem.cangoincontainer and
            not v.components.inventoryitem:IsHeld()) then
            return false
        end
    end
    if ignorethese ~= nil and ignorethese[v] ~= nil and ignorethese[v].worker ~= worker then
        return false
    end
    if onlytheseprefabs ~= nil and onlytheseprefabs[ispickable and v.components.pickable.product or v.prefab] == nil then
        return false
    end
    if v.components.container ~= nil then -- Containers are most likely sorted and placed by the player do not pick them up.
        return false
    end
    if v.components.bundlemaker ~= nil then -- Bundle creators are aesthetically placed do not pick them up.
        return false
    end
    if v.components.bait ~= nil and v.components.bait.trap ~= nil then -- Do not steal baits.
        return false
    end
    if v.components.trap ~= nil and not (v.components.trap:IsSprung() and v.components.trap:HasLoot()) then -- Only interact with traps that have something in it to take.
        return false
    end
    if not ispickable and owner.components.inventory:CanAcceptCount(v, 1) <= 0 then -- TODO(JBK): This is not correct for traps nor pickables but they do not have real prefabs made yet to check against.
        return false
    end
    if ba ~= nil and ba.target == v and (ba.action == _G.ACTIONS.PICKUP or ba.action == _G.ACTIONS.CHECKTRAP or ba.action == _G.ACTIONS.PICK) then
        return false
    end

    return v, ispickable
end

--Note: FindPickupableItemMyHome adapted for MyHome
--      from FindPickupableItem function in simutil.lua (last updated 06.06.2024)
--      owner = leader

-- This function looks for an item on the ground that could be _G.ACTIONS.PICKUP (or _G.ACTIONS.CHECKTRAP if a trap) by the owner and subsequently put into the owner's inventory.
function _G.FindPickupableItemMyHome(owner, radius, furthestfirst, positionoverride, ignorethese, onlytheseprefabs, allowpickables, worker, extra_filter)
    if owner == nil or owner.components.inventory == nil then
        return nil
    end
    local ba = owner:GetBufferedAction()
    local x, y, z
    if positionoverride then
        x, y, z = positionoverride:Get()
    else
        x, y, z = owner.Transform:GetWorldPosition()
    end
    local ents = _G.TheSim:FindEntities(x, y, z, radius, nil, PICKUP_CANT_TAGS, PICKUP_MUST_ONEOF_TAGS)
    local istart, iend, idiff = 1, #ents, 1
    if furthestfirst then
        istart, iend, idiff = iend, istart, -1
    end
    for i = istart, iend, idiff do
        local v = ents[i]
        local ispickable = v:HasTag("pickable")

        local entx, enty, entz = v.Transform:GetWorldPosition()
        local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(entx, enty, entz)
        if home ~= nil and not home:IsAvailableForDoer(owner) then
            v = nil
        end

        if v ~= nil and FindPickupableItem_filter(v, ba, owner, radius, furthestfirst, positionoverride, ignorethese, onlytheseprefabs, allowpickables, ispickable, worker, extra_filter) then
            return v, ispickable
        end
    end
    return nil, nil
end

if _G.TUNING.MYHOME.ROBOT_SEPARATE_AREAS_WORK then
    --Note: StorageRobotCommon adapted for MyHome from prefabs/storage_robot_common
    local StorageRobotCommon = require("prefabs/storage_robot_common")

    local ROBOT_PICKUP_MUST_TAGS =
    {
        "_inventoryitem"
    }
    local ROBOT_PICKUP_CANT_TAGS =
    {
        "INLIMBO", "NOCLICK", "irreplaceable", "knockbackdelayinteraction",
        "event_trigger", "mineactive", "catchable", "fire", "spider", "cursed",
        "heavy", "outofreach",
    }
    local ROBOT_CONTAINER_MUST_TAGS = { "_container" }
    local ROBOT_CONTAINER_CANT_TAGS = { "portablestorage", "mermonly", "mastercookware", "FX", "NOCLICK", "DECOR", "INLIMBO" }
    local ROBOT_ALLOWED_CONTAINER_TYPES = { "chest", "pack" }

    function StorageRobotCommon.FindContainerWithItem(inst, item, count)
        count = count or 0
        local sx, sy, sz = StorageRobotCommon.GetSpawnPoint(inst):Get()
        local shome = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(sx, sy, sz)
        local stack_maxsize = item.components.stackable ~= nil and item.components.stackable.maxsize or 1
        local ents = _G.TheSim:FindEntities(sx, sy, sz, _G.TUNING.STORAGE_ROBOT_WORK_RADIUS, ROBOT_CONTAINER_MUST_TAGS, ROBOT_CONTAINER_CANT_TAGS)

        local platform = inst:GetCurrentPlatform()

        for i, ent in ipairs(ents) do
            if ent.components.container ~= nil and
                table.contains(ROBOT_ALLOWED_CONTAINER_TYPES, ent.components.container.type) and
                (ent.components.container.canbeopened or ent.components.container.canacceptgivenitems) and -- NOTES(JBK): canacceptgivenitems is a mod flag for now.
                ent.components.container:Has(item.prefab, 1) and
                ent.components.container:CanAcceptCount(item, stack_maxsize) > count and
                ent:IsOnPassablePoint() and
                ent:GetCurrentPlatform() == platform
            then
                local cx, cy, cz = ent.Transform:GetWorldPosition()
                local chome = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(cx, cy, cz)
                if shome == chome then
                    return ent
                end
            end
        end
    end

    function StorageRobotCommon.FindItemToPickupAndStore(inst, match_item)
        local x, y, z    = inst.Transform:GetWorldPosition()
        local sx, xy, sz = StorageRobotCommon.GetSpawnPoint(inst):Get()
        local shome = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(sx, xy, sz)

        local ents = _G.TheSim:FindEntities(x, y, z, _G.TUNING.STORAGE_ROBOT_WORK_RADIUS, ROBOT_PICKUP_MUST_TAGS, ROBOT_PICKUP_CANT_TAGS)

        if shome ~= nil then
            for i, ent in ipairs(ents) do
                if ent:GetDistanceSqToPoint(sx, xy, sz) <= _G.TUNING.STORAGE_ROBOT_WORK_RADIUS * _G.TUNING.STORAGE_ROBOT_WORK_RADIUS then
                    local item, container = StorageRobotCommon.FindItemToPickupAndStore_filter(inst, ent, match_item)

                    if item ~= nil then
                        local ix, iy, iz = item.Transform:GetWorldPosition()
                        local ihome = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(ix, iy, iz)
                        if ihome == shome then
                            return item, container
                        else
                            return nil, nil
                        end
                    end
                end
            end
        else
            for i, ent in ipairs(ents) do
                if ent:GetDistanceSqToPoint(sx, xy, sz) <= _G.TUNING.STORAGE_ROBOT_WORK_RADIUS * _G.TUNING.STORAGE_ROBOT_WORK_RADIUS then
                    local item, container = StorageRobotCommon.FindItemToPickupAndStore_filter(inst, ent, match_item)

                    if item ~= nil then
                        local ix, iy, iz = item.Transform:GetWorldPosition()
                        local ihome = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(ix, iy, iz)
                        if ihome ~= nil then
                            return nil, nil
                        else
                            return item, container
                        end
                    end
                end
            end
        end
    end
end
