--English
GLOBAL.STRINGS.MYHOME =
{
    UNUSABLE = "Cannot be used.",
    THIS_IS_WALL_FRIEND = "This is the wall of a friend.",
    THIS_IS_WALL_ANOTHER = "This is the wall of other players.",
    THIS_IS_HOME_MY = "That's MyHome.",
    THIS_IS_HOME_FRIEND = "This is a friend's MyHome.",
    THIS_IS_HOME_ANOTHER = "This is the MyHome of other players.",
    HOME_MY = "This is my MyHome and can only be canceled from the beginning.",
    HOME_FRIEND = "This is the MyHome of %s, a friend of mine.",
    HOME_ANOTHER = "This is the MyHome of %s. I can't do that.\nAsk him to type #add%s so that he may have friendly relations with me.",
    WALL_FRIEND = "Process can only begin from my own wall, This is made by %s.",
    WALL_ANOTHER = "Process can only begin from my own wall, this is made by %s.\nAsk him to type #add%s so that he may have friendly relations with me.",
    LOCK = "Set this to MyHome.",
    UNLOCK = "Cancel MyHome.",
    FOCUS_WALL = "Select a wall to continue.",
    SET_DIRECTION = "Indicate the direction.",
    INFORMER_DEFAULT = "Selected %s walls, please continue.\nMy \\ Friends\n%s \\  %s",
    INFORMER_ADMIN = "Selected %s walls, please continue.\nMy \\ Friends \\ Others\n%s \\  %s  \\ %s",
    REQUEST_DESTROY = "Cancelling MyHome request is being sent...",
    PROCESS_TIMEOUT = "Timeout failed.",
    PROCESS_SUCCESS = "Conversion to MyHome successful.",
    PROCESS_FAIL = "Conversion to MyHome failed.",
    PROCESS_DESTROYED = "Cancelling MyHome successful.",
    ANNOUNCE_SUCCESS = "%s built a wall for the MyHome of %s.",
    ANNOUNCE_DESTROYED = "%s canceled a MyHome.",
    ANNOUNCE_GLOMMER_MURDER_IN_SHARD_WORLD = "Glommer in the world %s died from being trapped inside MyHome.",
    ANNOUNCE_GLOMMER_MURDER_IN_WORLD = "Glommer died from being trapped inside MyHome.",
    ANNOUNCE_CHESTER_MURDER_IN_SHARD_WORLD = "Chester in the world %s died from being trapped inside MyHome.",
    ANNOUNCE_CHESTER_MURDER_IN_WORLD = "Chester died from being trapped inside MyHome.",
    ANNOUNCE_HUTCH_MURDER_IN_SHARD_WORLD = "Hutch in the world %s died from being trapped inside MyHome.",
    ANNOUNCE_HUTCH_MURDER_IN_WORLD = "Hutch died from being trapped inside MyHome.",
    ANNOUNCE_WOBOT_KICK_OUT_IN_SHARD_WORLD = "W.O.B.O.T in the world %s is freed from being imprisoned inside MyHome.",
    ANNOUNCE_WOBOT_KICK_OUT_IN_WORLD = "W.O.B.O.T is freed from being imprisoned inside MyHome.",
    PLAYER_ADDED = "Added %s as a friendly relationship.",
    PLAYER_DID_FRIEND = "%s thinks me friendly.",
    PLAYER_FRIENDS = "I already have a friendly relationship with %s.",
    PLAYER_NOT_FRIENDS = "%s is not a friendly relationship.",
    PLAYER_DELETED = "Cancelled friendly relations with %s",
    PLAYER_CANCELED_FRIENDS = "%s is no longer friendly with me.",
    LOOP_INVALID = "[ERROR] Contour integrity error.",
    LOOP_NOT_FINISHED = "[ERROR] Contour must be looped.",
    ERROR_MIN_WALLS = "[ERROR] Conversion to MyHome requires at least %s walls.",
    ERROR_MAX_WALLS = "[ERROR] Conversion to MyHome cannot exceed %s walls.",
    ERROR_50PERCENT_WALLS = "[ERROR] Your friends' walls cannot be more than 50%.",
    INVALID_DISTANCE = "[ERROR] Too close to other MyHomes.",
    OUTSIDE_HOME_FORBIDDEN = "[ERROR] MyHome field of vision cannot have %s.",
    INSIDE_HOME_FORBIDDEN = "[ERROR] %s cannot be inside MyHome.",
    INSIDE_HOME60_FORBIDDEN = "[ERROR] %s cannot be inside MyHome.\nwith walls less than 60 pieces.",
    AREA_LABYRINTH_FORBIDDEN = "[ERROR] It is forbidden to conversion MyHome inside the labyrinth.",
    AREA_RUINS_FORBIDDEN = "[ERROR] It is forbidden to conversion MyHome inside the ruins.",
    AREA_ATRIUM_FORBIDDEN = "[ERROR] It is forbidden to conversion MyHome inside the atrium.",
    AREA_HERMITCRABISLAND_FORBIDDEN = "[ERROR] It is forbidden to conversion a MyHome on the island of the hermit crab.",
    AREA_ARCHIVEMAZE_FORBIDDEN = "[ERROR] It is forbidden to conversion MyHome inside the archive.",
    WALKABLE_PLATFORM_FORBIDDEN = "[ERROR] It is forbidden to conversion MyHome on a moving platform.",
    ICE_TILE_FORBIDDEN = "[ERROR] It is forbidden to conversion MyHome on ice tile.",
    MENU_BUTTON_MYHOME = "MyHome",
    TAB_TITLE_PLAYERS = "Players",
    TAB_TITLE_INFO = "Information",
    PAGE_PLAYERS_NONE = "There are no players registered in MyHome.",
    PAGE_PLAYERS_IN_WORLD = "in world",
    PAGE_PLAYERS_ONLINE = "online",
    PAGE_PLAYERS_OFFLINE = "offline",
    PAGE_PLAYERS_STATUS_FRIENDLY = "friend with me",
    PAGE_PLAYERS_STATUS_MY_FRIEND = "my friend",
    PAGE_PLAYERS_STATUS_FRIENDS = "friends together",
    PAGE_PLAYERS_HELP_ADD = "Add to friends",
    PAGE_PLAYERS_HELP_DEL = "Remove from friends",
    PAGE_INFO_ADMIN_ENABLED = "Administrator has privileges.",
    PAGE_INFO_ADMIN_DISABLED = "Administrator dont have privileges.",
    PAGE_INFO_ANNOUNCE_ENABLED = "The MyHome announcement is enabled.",
    PAGE_INFO_ANNOUNCE_DISABLED = "The MyHome announcement is disabled.",
    PAGE_INFO_MIN_WALLS_ADMIN = "Minimum number of walls in MyHome: %s (%s for administrator).",
    PAGE_INFO_MIN_WALLS = "Minimum number of walls in MyHome: %s.",
    PAGE_INFO_MAX_WALLS_ADMIN_ENABLED = "Maximum number of walls in MyHome: %s (unlimit for administrator).",
    PAGE_INFO_MAX_WALLS_ENABLED = "Maximum number of walls in MyHome: %s.",
    PAGE_INFO_MAX_WALLS_DISABLED = "Maximum number of walls in MyHome is unlimited.",
    PAGE_INFO_MIN_DIST_BETWEEN_MYHOMES_ADMIN_ENABLED = "Minimum distance between MyHome: %s (unlimit for administrator).",
    PAGE_INFO_MIN_DIST_BETWEEN_MYHOMES_ENABLED = "Minimum distance between MyHome: %s.",
    PAGE_INFO_MIN_DIST_BETWEEN_MYHOMES_DISABLED = "The distance between MyHome is unlimited .",
    PAGE_INFO_INTRUDERS_ENABLED = "Players are pushed out of someone else's MyHome.",
    PAGE_INFO_INTRUDERS_DISABLED = "Players are not pushed out of someone else's MyHome.",
    PAGE_INFO_RESTRICTED_AREA_ENABLED = "Enabled restriction on conversion of MyHome.",
    PAGE_INFO_RESTRICTED_AREA_DISABLED = "Disabled restriction on conversion of MyHome.",
    PAGE_INFO_ROBOT_SEPARATE_AREAS_WORK_ENABLED = "The division into zones of the robot work inside or outside MyHome is enabled.",
    PAGE_INFO_ROBOT_SEPARATE_AREAS_WORK_DISABLED = "The division into robot work zones inside or outside MyHome is disabled.",
    PAGE_INFO_PUSHOUT_COMING_BOSSES_ENABLED = "Coming bosses are pushed out of MyHome.",
    PAGE_INFO_PUSHOUT_COMING_BOSSES_DISABLED = "Coming bosses are not pushed out of MyHome.",
    PAGE_INFO_PUSHOUT_COMING_MOBS_ENABLED = "Coming mobs (hounds, worms, krampuses, etc) are pushed out of MyHome.",
    PAGE_INFO_PUSHOUT_COMING_MOBS_DISABLED = "Coming mobs (hounds, worms, krampuses, etc) are not pushed out of MyHome.",
    PAGE_INFO_PUSHOUT_HAUNTABLE_MOBS_ENABLED = "Hauntable mobs are pushed out of MyHome.",
    PAGE_INFO_PUSHOUT_HAUNTABLE_MOBS_DISABLED = "Hauntable mobs are not pushed out of MyHome.",
    PAGE_INFO_PUSHOUT_KLAUSSACK_ENABLED = "The bag of Klaus pushes out of MyHome",
    PAGE_INFO_PUSHOUT_KLAUSSACK_DISABLED = "The Claus bag does not push out of MyHome.",
    PAGE_INFO_ACCESS_RUINS_AREA_ENABLED = "It is allowed to conversion MyHome in ruins, labyrinth, atrium.",
    PAGE_INFO_ACCESS_RUINS_AREA_DISABLED = "It is forbidden to conversion MyHome in ruins, labyrinth, atrium.",
    PAGE_INFO_ACCESS_HERMITCRAB_ISLAND_ENABLED = "It is allowed to conversion MyHome on the island of the hermit crab.",
    PAGE_INFO_ACCESS_HERMITCRAB_ISLAND_DISABLED = "It is forbidden to conversion MyHome on Hermit crab Island.",
    PAGE_INFO_ACCESS_ARCHIVE_MAZE_ENABLED = "It is allowed to conversion MyHome in the archive.",
    PAGE_INFO_ACCESS_ARCHIVE_MAZE_DISABLED = "It is forbidden to conversion MyHome in the archive.",
    PAGE_INFO_GUEST_GLOMMER_DAYS_ALIVE = "Life time for Glommer inside MyHome: %s days.",
    PAGE_INFO_GUEST_GLOMMER_DAYS_ALIVE_DISABLED = "Life time for Glommer inside MyHome is unlimited.",
    PAGE_INFO_GUEST_CHESTER_DAYS_ALIVE = "Life time for Chester inside MyHome: %s days.",
    PAGE_INFO_GUEST_CHESTER_DAYS_ALIVE_DISABLED = "Life time for Chester inside MyHome is unlimited.",
    PAGE_INFO_GUEST_HUTCH_DAYS_ALIVE = "Life time for Hutch inside MyHome: %s days.",
    PAGE_INFO_GUEST_HUTCH_DAYS_ALIVE_DISABLED = "Life time for Hutch inside MyHome is unlimited.",
    PAGE_INFO_GUEST_WOBOT_DAYS_ALIVE = "W.O.B.O.T lifetime inside MyHome: %s days.",
    PAGE_INFO_GUEST_WOBOT_DAYS_ALIVE_DISABLED = "W.O.B.O.T's lifetime inside MyHome is unlimited.",
    PAGE_INFO_GUEST_WOBOT_FORCE_PUSHOUT = "W.O.B.O.T is forcibly cick from MyHome.",
}
