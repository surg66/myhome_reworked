--------------------------------------------------------------------------
--[[ MyHome Manager class definition ]]
--------------------------------------------------------------------------

return Class(function(self, inst)
--------------------------------------------------------------------------
--[[ Constants ]]
--------------------------------------------------------------------------
local SKIP_TESTING = 0
local ALLOW_TESTING = 1

--------------------------------------------------------------------------
--[[ Member variables ]]
--------------------------------------------------------------------------
--Public
self.inst = inst

--Private
local _world         = TheWorld
local _ismastersim   = _world.ismastersim
local _userids       = {}
local _names         = {}
local _friends       = {}
local _hashindexes
local _lasthomeid
local _maxradius
local _homes
local _activeplayers
local _junk_pile

if _ismastersim then
    _maxradius = 0
    _lasthomeid  = 0
    _hashindexes = {}
    _homes = {}
    _activeplayers = {}
    _junk_pile = nil
end

--Network
local _netvars =
{
    numusers = net_int(inst.GUID, "myhome._netvars.numusers"),
    friends_packet = net_string(inst.GUID, "myhome._netvars.friends_packet", "friendsdirty"),
    userids_json = net_string(inst.GUID, "myhome._netvars.userids_json", "useridsdirty"),
    names_json = net_string(inst.GUID, "myhome._netvars.names_json", "namesdirty"),
    inworld_players = {}
}

for i = 1, TheNet:GetServerMaxPlayers() do
    table.insert(_netvars.inworld_players, net_string(inst.GUID, "myhome._netvars.inworld_players."..i, "inworld_playersdirty"))
end

--------------------------------------------------------------------------
--[[ Private member functions ]]
--------------------------------------------------------------------------
local TalkerSay = _ismastersim and function(talker, message, seconds)
    if talker and talker:IsValid() and talker.components.talker then
        talker.components.talker:Say(message, seconds or 3)
    end
end

local UpdateNetVars = _ismastersim and function()
    --encode packet
    local packet = ""
    for k1, v1 in pairs(_friends) do
        packet = packet.."|"..k1
        for k2, v2 in pairs(v1) do
            packet = packet..":"..k2
        end
    end
    if packet ~= "" then
        packet = string.sub(packet, 2)
    end

    _netvars.numusers:set(#_userids)
    _netvars.friends_packet:set(packet)
    _netvars.userids_json:set(json.encode(_userids))
    _netvars.names_json:set(json.encode(_names))
    _world:PushEvent("myhome_playerspagerefresh")
end or nil

local SortHomes = _ismastersim and function()
    _maxradius = 0
    --pass 1
    for k, _ in pairs(_homes) do
        _homes[k] = ALLOW_TESTING
        if k._radius > _maxradius then
            _maxradius = k._radius
        end
    end
    --pass 2
    for k1, _ in pairs(_homes) do
        for k2, _ in pairs(_homes) do
            if k1 ~= k2 and k1._polygon ~= nil and k2.polygon ~= nil then
                if TheSim:WorldPointInPoly(k1._polygon[1][1], k1._polygon[1][2], k2.polygon) then
                    _homes[k1] = SKIP_TESTING
                end
            end
        end
    end
    --pass 3
    for k, v in pairs(_homes) do
        if v == ALLOW_TESTING then
            k:SetTesting(true)
        else
            k:SetTesting(false)
        end
    end
end or nil

--------------------------------------------------------------------------
--[[ Private event handlers ]]
--------------------------------------------------------------------------
local OnPlayerJoined = _ismastersim and function(world, player)
    for i, v in ipairs(_activeplayers) do
        if v == player then
            return
        end
    end

    table.insert(_activeplayers, player)

    for i, v in ipairs(_netvars.inworld_players) do
        if v:value() == "" then
            _netvars.inworld_players[i]:set(player.userid)
            break
        end
    end

    self:UpdateUserName(player)

    local userindex = self:GetUserIndexByPlayer(player)
    if userindex == 0 and TUNING.MYHOME.ADMIN and player.Network:IsServerAdmin() then
        userindex = self:AddUser(player)
    end

    if userindex > 0 then
        local tag = "mh_u"..userindex
        if not player:HasTag(tag) then
            player:AddTag(tag)
        end

        player:DoTaskInTime(0, function()
            local x, y, z = player.Transform:GetWorldPosition()
            local home = self:GetHomeByPosition(x, y, z)
            if home ~= nil and not home:IsAvailableForDoer(player) then
                home:DoPushForce(player)
            end
        end)
    end
end or nil

local OnPlayerLeft = _ismastersim and function(world, player)
    for i, v in ipairs(_activeplayers) do
        if v == player then
            for i, v in ipairs(_netvars.inworld_players) do
                if v:value() == player.userid then
                    _netvars.inworld_players[i]:set("")
                    break
                end
            end

            table.remove(_activeplayers, i)
            return
        end
    end
end or nil

local OnRegisterHome = _ismastersim and function(world, ent)
    if ent ~= nil and ent:IsValid() then
        if _homes[ent] == nil then
            _homes[ent] = ALLOW_TESTING
            SortHomes()
        end
    end
end or nil

local OnUnregisterHome = _ismastersim and function(world, ent)
    if ent ~= nil and ent:IsValid() then
        if _homes[ent] ~= nil then
            _homes[ent] = nil
            SortHomes()
        end
    end
end or nil

local OnFriendsDirty = not _ismastersim and function()
    _friends = {}
    --decode packet
    for text in string.gmatch(_netvars.friends_packet:value(), "[^|]+") do
        local key = 0
        for n in string.gmatch(text, "[^:]+") do
            if key == 0 then
                key = tonumber(n)
                _friends[key] = {}
            else
                _friends[key][tonumber(n)] = true
            end
        end
    end
    _world:PushEvent("myhome_playerspagerefresh")
end or nil

local OnUserIdsDirty = not _ismastersim and function()
    _userids = json.decode(_netvars.userids_json:value())
    _world:PushEvent("myhome_playerspagerefresh")
end or nil

local OnNamesDirty = not _ismastersim and function()
    _names = json.decode(_netvars.names_json:value())
    _world:PushEvent("myhome_playerspagerefresh")
end or nil

local OnInWorldPlayersDirty = not _ismastersim and function()
    _world:PushEvent("myhome_playerspagerefresh")
end or nil

--------------------------------------------------------------------------
--[[ Initialization ]]
--------------------------------------------------------------------------
_netvars.numusers:set(0)
_netvars.friends_packet:set("")
_netvars.userids_json:set("{}")
_netvars.names_json:set("{}")

for i, v in ipairs(_netvars.inworld_players) do
    v:set("")
end

if _ismastersim then
    inst:ListenForEvent("ms_playerjoined", OnPlayerJoined, _world)
    inst:ListenForEvent("ms_playerleft", OnPlayerLeft, _world)
    inst:ListenForEvent("myhome_register", OnRegisterHome, _world)
    inst:ListenForEvent("myhome_unregister", OnUnregisterHome, _world)
else
    inst:ListenForEvent("friendsdirty", OnFriendsDirty)
    inst:ListenForEvent("namesdirty", OnNamesDirty)
    inst:ListenForEvent("useridsdirty", OnUserIdsDirty)
    inst:ListenForEvent("inworld_playersdirty", OnInWorldPlayersDirty)
end

--------------------------------------------------------------------------
--[[ Public member functions ]]
--------------------------------------------------------------------------
function self:GetNumUsers()
    return _netvars.numusers:value()
end

function self:GetFriends()
    return _friends
end

function self:GetNames()
    return _names
end

function self:GetUserIds()
    return _userids
end

function self:InWorld(userid)
    for _, v in ipairs(_netvars.inworld_players) do
        if v:value() == userid then
            return true
        end
    end
    return false
end

function self:IsFriend(userindex, targetindex)
    if _friends[userindex] and _friends[userindex][targetindex] then
        return true
    end
    return false
end

function self:GetNameByIndex(index)
    local name = _names[index]
    if type(name) == "string" then
        return name
    end
    return ""
end

function self:GetUserIndexFromTag(player)
    if player then
        local numusers = _netvars.numusers:value()
        for i = 1, numusers do
            if player:HasTag("mh_u"..i) then
                return i
            end
        end
    end
    return 0
end

if _ismastersim then function self:GetUserIndexByPlayer(player)
    local userindex = _hashindexes[player.userid]
    if userindex ~= nil then
        return userindex
    end
    return 0
end end

if _ismastersim then function self:GetPlayerById(userid)
    for _, v in ipairs(AllPlayers) do
        if v ~= nil and v.userid and v.userid == userid then
            return v
        end
    end
    return nil
end end

if _ismastersim then function self:GetPlayerByUserIndex(index)
    for _, v in ipairs(AllPlayers) do
        if v ~= nil and v.userid then
            local userindex = _hashindexes[v.userid]
            if userindex ~= nil and userindex == index then
                return v
            end
        end
    end
    return nil
end end

if _ismastersim then function self:GetScoreIndexByPlayer(player)
    local isdedicated = not TheNet:GetServerIsClientHosted()
    local scoreindex = 1
    for _, v in ipairs(TheNet:GetClientTable()) do
        if not isdedicated or v.performance == nil then
            if v.userid == player.userid then
                break
            end
            scoreindex = scoreindex + 1
        end
    end
    return scoreindex
end end

if _ismastersim then function self:GetPlayerByScoreIndex(scoreindex)
    local isdedicated = not TheNet:GetServerIsClientHosted()
    local index = 1
    for _, v in ipairs(TheNet:GetClientTable()) do
        if not isdedicated or v.performance == nil then
            if index == scoreindex then
                return self:GetPlayerById(v.userid)
            end
            index = index + 1
        end
    end
    return nil
end end

if _ismastersim then function self:GetMaxRadius()
    return _maxradius
end end

if _ismastersim then function self:AddUser(player)
    local userindex = _hashindexes[player.userid]
    if userindex == nil then
        table.insert(_userids, player.userid)
        table.insert(_names, player.name)
        userindex = #_userids
        _hashindexes[player.userid] = userindex
        _friends[userindex] = nil
        player:AddTag("mh_u"..userindex)
        UpdateNetVars()
    end
    return userindex
end end

if _ismastersim then function self:AddUserClient(userid, name)
    local userindex = _hashindexes[userid]
    if userindex == nil then
        table.insert(_userids, userid)
        table.insert(_names, name)
        userindex = #_userids
        _hashindexes[userid] = userindex
        _friends[userindex] = nil
        UpdateNetVars()
    end
    return userindex
end end

if _ismastersim then function self:AddFriend(userindex1, userindex2)
    if _friends[userindex1] == nil then
        _friends[userindex1] = {}
    end
    _friends[userindex1][userindex2] = true
    UpdateNetVars()
end end

if _ismastersim then function self:DeleteFriend(userindex1, userindex2)
    if _friends[userindex1] ~= nil then
        _friends[userindex1][userindex2] = nil
        UpdateNetVars()
    end
end end

if _ismastersim then function self:CommandFriends(command, doer, target_userid)
    if command ~= "add" and command ~= "del" and target_userid ~= "" then return end

    if doer.userid == target_userid then
        TalkerSay(doer, STRINGS.MYHOME.UNUSABLE)
        return
    end

    local target = self:GetPlayerById(target_userid)
    local target_userindex = _hashindexes[target_userid]
    local target_name = ""

    if target_userindex == nil then
        local client = TheNet:GetClientTableForUser(target_userid)
        if client ~= nil then
            target_name = client.name
        else
            TalkerSay(doer, STRINGS.MYHOME.UNUSABLE)
            return
        end
    else
        target_name = self:GetNameByIndex(target_userindex)
    end

    if target ~= nil then
        target_name = target:GetDisplayName()
    end

    local doer_userindex = self:GetUserIndexByPlayer(doer)
    if doer_userindex == 0 then
        doer_userindex = self:AddUser(doer)
    end

    if target_userindex == nil then
        if target ~= nil then
            target_userindex = self:AddUser(target)
        else
            target_userindex = self:AddUserClient(target_userid, target_name)
        end
    end

    if command == "add" then
        if not self:IsFriend(doer_userindex, target_userindex) then
            self:AddFriend(doer_userindex, target_userindex)
            TalkerSay(doer, string.format(STRINGS.MYHOME.PLAYER_ADDED, target_name))
            if target ~= nil then
                TalkerSay(target, string.format(STRINGS.MYHOME.PLAYER_DID_FRIEND, doer:GetDisplayName()))
            end
        else
            TalkerSay(doer, string.format(STRINGS.MYHOME.PLAYER_FRIENDS, target_name))
        end
    elseif command == "del" then
        if self:IsFriend(doer_userindex, target_userindex) then
            self:DeleteFriend(doer_userindex, target_userindex)
            TalkerSay(doer, string.format(STRINGS.MYHOME.PLAYER_DELETED, target_name))
            if target ~= nil then
                TalkerSay(target, string.format(STRINGS.MYHOME.PLAYER_CANCELED_FRIENDS, doer:GetDisplayName()))
            end
        else
            TalkerSay(doer, string.format(STRINGS.MYHOME.PLAYER_NOT_FRIENDS, target_name))
        end
    end
end end

if _ismastersim then function self:UpdateUserName(player)
    local userindex = self:GetUserIndexByPlayer(player)
    if userindex > 0 and _names[userindex] ~= nil and _names[userindex] ~= player.name then
        _names[userindex] = player.name
        UpdateNetVars()
    end
end end

if _ismastersim then function self:GenerateHomeID()
    _lasthomeid = _lasthomeid + 1
    return _lasthomeid
end end

if _ismastersim then function self:GetHomeByPosition(x, y, z, isrect, outdist)
    for k, v in pairs(_homes) do
        if v == ALLOW_TESTING and k:IsPointInside(x, y, z, isrect, outdist) then
            return k
        end
    end
    return nil
end end

if _ismastersim then function self:GetHomeByHomeIndex(index)
    for k, v in pairs(_homes) do
        if k._homeindex == index then
            return k
        end
    end
    return nil
end end

if _ismastersim then function self:RegisterJunkPileInWorld(inst)
    _junk_pile = inst
end end

if _ismastersim then function self:GetJunkPileInWorld()
    return _junk_pile
end end

if _ismastersim then function self:OnDeployWall(target, doer)
    if target and doer and doer:HasTag("player") and not doer:HasTag("playerghost") then
        local userindex = self:GetUserIndexByPlayer(doer)
        if userindex == 0 then
            userindex = self:AddUser(doer)
        end
        target.components.myhome_wall:SetOwnerByIndex(userindex)
    end
end end

if _ismastersim then function self:DoBuild(target, doer)
    if target and doer and doer:HasTag("player") and
       not doer:HasTag("playerghost") and not doer:HasTag("mh_captor") then
        local userindex = self:GetUserIndexByPlayer(doer)
        if userindex > 0 and
           ((TUNING.MYHOME.ADMIN and doer.Network:IsServerAdmin()) or
            (userindex == target.components.myhome_wall.ownerindex)) then
            local builder = SpawnPrefab("myhome_builder")
            if builder ~= nil then
                local x, _, z = target.Transform:GetWorldPosition()
                builder.Transform:SetPosition(x, 0, z)
                builder:LinkToPlayer(doer)
                builder:LinkToTarget(target)
            end
        end
    end
end end

if _ismastersim then function self:DoDestroy(target, doer)
    if target and doer and doer:HasTag("player") and
       not doer:HasTag("playerghost") and not doer:HasTag("mh_captor") then
        local userindex = self:GetUserIndexByPlayer(doer)
        if userindex > 0 and target.components.myhome_wall.homeindex > 0 and
           ((TUNING.MYHOME.ADMIN and doer.Network:IsServerAdmin()) or
            (userindex == target.components.myhome_wall.ownerindex)) then
            local homeindex = target.components.myhome_wall.homeindex
            local home = self:GetHomeByHomeIndex(homeindex)
            if home ~= nil then
                home:DoDestroy(doer)
            end
        end
    end
end end

if _ismastersim then function self:DoFocusWall(target, doer)
    if target and doer and doer:HasTag("player") and
       not doer:HasTag("playerghost") and doer:HasTag("mh_captor") then
        local userindex = self:GetUserIndexByPlayer(doer)
        if userindex > 0 and
           ((TUNING.MYHOME.ADMIN and doer.Network:IsServerAdmin()) or
            (userindex == target.components.myhome_wall.ownerindex) or
            (self:IsFriend(target.components.myhome_wall.ownerindex, userindex))) then
            TheWorld:PushEvent("myhome_focuswall", {doer = doer, wall = target})
        end
    end
end end

if _ismastersim then function self:DoShowCtrl(target, doer)
    if target and doer and doer:HasTag("player") and
       not doer:HasTag("playerghost") and not doer:HasTag("mh_captor") then
        if target.components.myhome_wall.homeindex > 0 then
            local userindex = self:GetUserIndexByPlayer(doer)
            local homeindex = target.components.myhome_wall.homeindex
            local home = self:GetHomeByHomeIndex(homeindex)
            if home ~= nil then
                local ctrl = home:GetCtrl()
                if ctrl ~= nil then
                    local ownerindex = ctrl.components.myhome_wall.ownerindex
                    local ownername = self:GetNameByIndex(ownerindex)
                    if userindex > 0 and userindex == ownerindex then
                        TalkerSay(doer, STRINGS.MYHOME.HOME_MY)
                    elseif userindex > 0 and ownerindex > 0 and self:IsFriend(ownerindex, userindex) then
                        TalkerSay(doer, string.format(STRINGS.MYHOME.HOME_FRIEND, ownername))
                    elseif ownerindex > 0 then
                        local scoreindex = self:GetScoreIndexByPlayer(doer)
                        TalkerSay(doer, string.format(STRINGS.MYHOME.HOME_ANOTHER, ownername, tostring(scoreindex)))
                    end
                end
                home:DoShowCtrl(doer)
            end
        end
    end
end end

--------------------------------------------------------------------------
--[[ Save/Load ]]
--------------------------------------------------------------------------
if _ismastersim then function self:OnSave()
    return {compatibility = 1, lasthomeid = _lasthomeid, userids = _userids, names = _names, friends = _friends}
end end

if _ismastersim then function self:OnLoad(data)
    if data then
        if data.compatibility == 1 then
            if data.lasthomeid then _lasthomeid = data.lasthomeid end
            if data.names then _names = data.names end
            if data.userids then
                _userids = data.userids
                for i, v in ipairs(_userids) do
                    _hashindexes[v] = i
                end
            end
            if data.friends then _friends = data.friends end
        end
        UpdateNetVars()
    end
end end

--------------------------------------------------------------------------
--[[ Debug ]]
--------------------------------------------------------------------------
function self:GetDebugString()
    local str = "[client data]\n"
    local count = 0
    if _ismastersim then
        str = "[server data]\n"
        str = str.."lasthomeid: ".._lasthomeid.."\n"
    end

    str = str.."numusers: ".._netvars.numusers:value()

    if _ismastersim then
        str = str.."\nhashindexes:"
        count = 0
        for k, v in pairs(_hashindexes) do
            count = count + 1
            if count >= 8 then
                count = 0
                str = str.." "..v.."-"..k.."\n"
            else
                str = str.." "..v.."-"..k
            end
        end
    end

    str = str.."\nuserids:"
    count = 0
    for i, v in ipairs(_userids) do
        count = count + 1
        if count >= 8 then
            count = 0
            str = str.." "..i.."-"..v.."\n"
        else
            str = str.." "..i.."-"..v
        end
    end

    str = str.."\nnames:"
    count = 0
    for i, v in ipairs(_names) do
        count = count + 1
        if count >= 8 then
            count = 0
            str = str.." "..i.."-"..v.."\n"
        else
            str = str.." "..i.."-"..v
        end
    end

    str = str.."\nfriends:"
    for i, f in pairs(_friends) do
        str = str.." "..i.."-["
        local space = ""
        for k, v in pairs(f) do
            if v then
                str = str..space..k
                if space == "" then space = " " end
            end
        end
        str = str.."]"
    end

    str = str.."\nin world:"
    for i, v in ipairs(_netvars.inworld_players) do
        if v:value() ~= "" then
            str = str.." "..v:value()
        end
    end

    return str
end

--------------------------------------------------------------------------
--[[ End ]]
--------------------------------------------------------------------------
end)
