local StorageRobotCommon = require("prefabs/storage_robot_common")

local TYPE_ANIMAL = "animal"
local TYPE_WOBOT = "wobot"
local JUNK_PILE_SIDE_RADIUS = 5

local MyHome_GuestObserver = Class(function(self, inst)
    self.inst = inst
    self.guest = false
    self.type = TYPE_ANIMAL
    self.limit = 0
    self.timer = 0
    self.percent = 0
    self.onmurderfn = nil
    self.onmovetojunkpilefn = nil
    self.onkickoutfn = nil

    self.inst:DoTaskInTime(0, function() self:Reset() end)
end)

function MyHome_GuestObserver:SetLimit(limit)
    self.limit = limit
end

function MyHome_GuestObserver:IsLimit()
    if self.timer >= self.limit then
        return true
    end
    return false
end

function MyHome_GuestObserver:IsGuest()
    return self.guest
end

function MyHome_GuestObserver:SetOnMurderFn(fn)
    self.onmurderfn = fn
end

function MyHome_GuestObserver:SetOnMoveToJunkPileFn(fn)
    self.onmovetojunkpilefn = fn
end

function MyHome_GuestObserver:SetOnKickOutFn(fn)
    self.onkickoutfn = fn
end

function MyHome_GuestObserver:SetTypeAnimal()
    self.type = TYPE_ANIMAL
end

function MyHome_GuestObserver:SetTypeWOBOT()
    self.type = TYPE_WOBOT
end

function MyHome_GuestObserver:StopUpdate()
    if self.task ~= nil then
        self.task:Cancel()
        self.task = nil
    end
end

function MyHome_GuestObserver:Update()
    local currtime = GetTime()
    local dt = currtime - self.oldtime
    local x, y, z = self.inst.Transform:GetWorldPosition()
    local maxradius = TheWorld.net.components.myhome_manager:GetMaxRadius()
    local ents = TheSim:FindEntities(x, y, z, maxradius, {"myhome"})
    local found_home = nil
    local leader = nil

    self.guest = false

    for _, v in ipairs(ents) do
        found_home = v
        if found_home:IsPointInside(x, y, z) then
            self.guest = true
            break
        end
    end

    if self.type == TYPE_ANIMAL then
        if self.inst.components.follower then
            leader = self.inst.components.follower:GetLeader()
        end

        if not self.guest and leader ~= nil then
            local lx, ly, lz = leader.Transform:GetWorldPosition()
            ents = TheSim:FindEntities(lx, ly, lz, maxradius, {"myhome"})
            for _, v in ipairs(ents) do
                found_home = v
                if found_home:IsPointInside(lx, ly, lz) then
                    self.guest = true
                    break
                end
            end
        end
    end

    if self.guest then
        self.timer = self.timer + dt
        if self.timer >= self.limit then
            self.timer = self.limit
        end
        if self.limit ~= 0 then
            self.percent = self.timer / self.limit
        end
    end

    if self.inst.mh_guestmarker ~= nil then
        self.inst.mh_guestmarker:SetPercent(self.percent)

        if self.guest then
            self.inst.mh_guestmarker:Show()
        else
            self.inst.mh_guestmarker:Hide()
        end
    end

    if self.guest and self:IsLimit() and found_home ~= nil then
        if self.type == TYPE_ANIMAL then
            if self.inst.components.health then
                if self.inst.mh_guestmarker ~= nil then
                    self.inst.mh_guestmarker:Hide()
                end

                if leader ~= nil then
                    found_home:DoPushForce(leader)
                end

                self.inst.components.health:Kill()

                if self.onmurderfn ~= nil then
                    self.onmurderfn(self.inst)
                end

                self:StopUpdate()
            end
        elseif self.type == TYPE_WOBOT then
            local x, _, z = self.inst.Transform:GetWorldPosition()
            local junk_pile_inst = TheWorld.net.components.myhome_manager:GetJunkPileInWorld()

            if self.inst.components.inventoryitem then
                local owner = self.inst.components.inventoryitem.owner
                if owner ~= nil then
                    if owner.components.inventory ~= nil then
                        owner.components.inventory:DropItem(self.inst)
                    elseif owner.components.container ~= nil then
                        owner.components.container:DropItem(self.inst)
                    end
                else
                    local fx = SpawnPrefab("spawn_fx_medium")
                    if fx ~= nil then
                        fx.Transform:SetPosition(x, 0, z)
                    end
                end
            end

            if self.inst.components.fueled then
                self.inst.components.fueled:SetPercent(0)
            end

            self.inst.sg:GoToState("idle_broken")

            if junk_pile_inst ~= nil and junk_pile_inst:IsValid() then                
                local jpx, jpy, jpz = junk_pile_inst.Transform:GetWorldPosition()
                local angle = math.random(1, 360) * DEGREES
                local tx = jpx + (JUNK_PILE_SIDE_RADIUS * math.cos(angle))
                local tz = jpz + (-JUNK_PILE_SIDE_RADIUS * math.sin(angle))

                if self.inst.Physics ~= nil then
                    self.inst.Physics:Teleport(tx, 0, tz)
                else
                    self.inst.Transform:SetPosition(tx, 0, tz)
                end
            else
                found_home:DoPushForce(self.inst)
            end

            StorageRobotCommon.UpdateSpawnPoint(self.inst)

            if self.inst._sleeptask ~= nil and not self.inst:IsAsleep() then
                self.inst._sleeptask:Cancel()
                self.inst._sleeptask = nil
            end

            local nx, _, nz = self.inst.Transform:GetWorldPosition()
            local fx = SpawnPrefab("spawn_fx_medium")
            if fx ~= nil then
                fx.Transform:SetPosition(nx, 0, nz)
            end

            if junk_pile_inst ~= nil then
                if self.onmovetojunkpilefn ~= nil then
                    self.onmovetojunkpilefn(self.inst)
                end
            else
                if self.onkickoutfn ~= nil then
                    self.onkickoutfn(self.inst)
                end
            end

            self.timer = 0            
            self.percent = 0
        end
    end

    self.oldtime = currtime
end

function MyHome_GuestObserver:Reset(iszerotimer)
    self:StopUpdate()

    self.oldtime = GetTime()

    if iszerotimer == true then
        self.timer = 0
    end

    if self.inst.mh_guestmarker == nil then
        self.inst.mh_guestmarker = SpawnPrefab("myhome_guestmarker")
        self.inst.mh_guestmarker.entity:SetParent(self.inst.entity)
    end

    --skip 15 frames, don't want spam every frames
    self.task = self.inst:DoPeriodicTask(15 * FRAMES, function() self:Update() end)
    self:Update()
end

function MyHome_GuestObserver:OnSave()
    local data = {timer = self.timer}
    return data
end

function MyHome_GuestObserver:OnLoad(data)
    if data ~= nil then
        if data.timer ~= nil then
            self.timer = data.timer
        end
    end
end

function MyHome_GuestObserver:GetDebugString()
    return string.format("%s %.2f/%.2f - %.2f", (self.guest and "guest" or "not guest"), self.timer, self.limit, self.percent)
end

return MyHome_GuestObserver
