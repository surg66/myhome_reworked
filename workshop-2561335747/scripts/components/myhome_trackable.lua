local MyHome_Trackable = Class(function(self, inst)
    self.inst = inst
    self.testforce = false
    self.optfx = {old = "small_puff", new = "small_puff"}
    self.mindistwall = 4
    self.oncanpushfn = nil
    self.onsuccesspushfn = nil
    self.onfailpushfn = nil
    self.onteleportedinsidefn = nil
    self.onaftertestforcefn = nil
    self.onteleported = function(ent)
        local x, y, z = ent.Transform:GetWorldPosition()
        local home = TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)
        if home ~= nil then
            if self.onteleportedinsidefn ~= nil then
                self.onteleportedinsidefn(ent, home)
            end
        end
    end

    self.inst:AddTag("mh_tracked")
    self.inst:ListenForEvent("teleported", self.onteleported)
    self.inst:DoTaskInTime(0, function(ent)
        if ent.components.myhome_trackable.testforce == true then
            ent.components.myhome_trackable:DoTestForce()
        end
    end)
end)

function MyHome_Trackable:Pause()
    if not self.inst:HasTag("mh_trackedpause") then
       self.inst:AddTag("mh_trackedpause")
    end
end

function MyHome_Trackable:Unpause()
    if self.inst:HasTag("mh_trackedpause") then
       self.inst:RemoveTag("mh_trackedpause")
    end
end

function MyHome_Trackable:SetOnAfterTestForceFn(fn)
    self.onaftertestforcefn = fn
end

function MyHome_Trackable:SetOnTeleportedInsideFn(fn)
    self.onteleportedinsidefn = fn
end

function MyHome_Trackable:SetOnCanPushFn(fn)
    self.oncanpushfn = fn
end

function MyHome_Trackable:SetOnSuccessPushFn(fn)
    self.onsuccesspushfn = fn
end

function MyHome_Trackable:SetOnFailPushFn(fn)
    self.onfailpushfn = fn
end

function MyHome_Trackable:OnCanPush(fromhome)
    if self.oncanpushfn ~= nil then
        local res = true
        if not self.oncanpushfn(self.inst, fromhome) then
            res = false
        end
        return res
    end
    return (not self.inst:HasTag("mh_trackedpause"))
end

function MyHome_Trackable:OnSuccessPush(oldpos, newpos)
    if self.onsuccesspushfn ~= nil then
        self.onsuccesspushfn(self.inst, oldpos, newpos)
    end
end

function MyHome_Trackable:OnFailPush(fromhome, pos)
    if self.onfailpushfn ~= nil then
        self.onfailpushfn(self.inst, fromhome, pos)
    end
end

--Note: ignores the player
function MyHome_Trackable:DoTestForce()
    if not self.inst:HasTag("player") then
        local x, y, z = self.inst.Transform:GetWorldPosition()
        local home = TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)
        if home ~= nil then
            home:DoPushForce(self.inst)
        end
        if self.onaftertestforcefn ~= nil then
            self.onaftertestforcefn(self.inst)
        end
    end
end

function MyHome_Trackable:OnSave()
    local data =
    {
        pause = false
    }

    if self.inst:HasTag("mh_trackedpause") then
        data.pause = true
    end

    return data
end

function MyHome_Trackable:OnLoad(data)
    if data ~= nil and data.pause then
        self.inst:AddTag("mh_trackedpause")
    end
end

function MyHome_Trackable:OnRemoveEntity()
    self.inst:RemoveTag("mh_tracked")
    self.inst:RemoveTag("mh_trackedpause")
    self.inst:RemoveEventCallback("teleported", self.onteleported)
end

MyHome_Trackable.OnRemoveFromEntity = MyHome_Trackable.OnRemoveEntity

function MyHome_Trackable:GetDebugString()
    local str = "pause: false"
    if self.inst:HasTag("mh_trackedpause") then
        str = "pause: true"
    end
    return str
end

return MyHome_Trackable
