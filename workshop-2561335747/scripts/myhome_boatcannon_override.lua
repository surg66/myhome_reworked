local _G = GLOBAL

local function Shoot(self, owner)
    if self.loadedammo == nil then
        return
    end

    local x, y, z = self.inst.Transform:GetWorldPosition()
    local projectile = _G.SpawnPrefab(self.loadedammo)
    if projectile == nil then
        self:LoadAmmo(nil)
        return
    end

    local theta = self.inst.Transform:GetRotation()* _G.DEGREES
    local radius = 0.5
    local offset = _G.Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))

    projectile.Transform:SetPosition(x + offset.x, y + _G.TUNING.BOAT.BOATCANNON.PROJECTILE_INITIAL_HEIGHT, z + offset.z)

    projectile.shooter = self.inst
    projectile.mh_owner = owner --for MyHome

    local angle = -self.inst.Transform:GetRotation() * _G.DEGREES
    local range = _G.TUNING.BOAT.BOATCANNON.RANGE

    -- Apply direction & power to shot
    local targetpos = _G.Vector3(x + math.cos(angle) * range, y, z + math.sin(angle) * range)
    projectile.components.complexprojectile:Launch(targetpos, self.inst, self.inst)

    -- Remove cannon ammo reference
    self:LoadAmmo(nil)

    -- Add a shot recoil to the boat
    local force_direction = -_G.Vector3(math.cos(angle), 0, math.sin(angle))
    local force = 1
    local boat = self.inst:GetCurrentPlatform()
    if boat ~= nil then
        boat.components.boatphysics:ApplyForce(force_direction.x, force_direction.z, force)
    end
end

AddComponentPostInit("boatcannon", function(self)
    -- Note: override original function Shoot
    self.Shoot = Shoot
end)
