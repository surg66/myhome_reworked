local function SetIcon(inst, icon)
    if icon == 1 then
        inst.AnimState:OverrideSymbol("swap_iconsmall", "myhome_markers", "icon1")
    elseif icon == 2 or icon == 3 then
        inst.AnimState:OverrideSymbol("swap_iconsmall", "myhome_markers", "icon2")
    elseif icon == 4 then
        inst.AnimState:OverrideSymbol("swap_iconsmall", "myhome_markers", "icon3")
    elseif icon == 5 then
        inst.AnimState:OverrideSymbol("swap_iconbig", "myhome_markers", "direction")
    elseif icon == 6 then
        inst.AnimState:OverrideSymbol("swap_iconbig", "myhome_markers", "protected")
    elseif icon == 7 then
        inst.AnimState:PlayAnimation("lock")
    elseif icon == 8 then
        inst.AnimState:PlayAnimation("unlock")
    end
end

local function marker()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
    
    inst.AnimState:SetBank("myhome_markers")
    inst.AnimState:SetBuild("myhome_markers")
    inst.AnimState:PlayAnimation("error")
    inst.AnimState:OverrideSymbol("swap_iconbig", "myhome_markers", "ignore")

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    inst.SetIcon = SetIcon

    inst:ListenForEvent("animover", inst.Remove)
    -- In case we're off screen and animation is asleep
    inst:DoTaskInTime(inst.AnimState:GetCurrentAnimationLength(), inst.Remove)

    return inst
end

local function guide()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("myhome_markers")
    inst.AnimState:SetBuild("myhome_markers")
    inst.AnimState:PlayAnimation("guide", true)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    inst.AnimState:SetTime(math.random() * inst.AnimState:GetCurrentAnimationLength())

    return inst
end

return Prefab("myhome_marker", marker),
       Prefab("myhome_guidemarker", guide)
