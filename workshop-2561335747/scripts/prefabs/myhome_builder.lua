local prefabdefs = require("myhome_prefabdefs")

local TIMEOUT = 15
--walls
local WALL_ANOTHER = 0
local WALL_MY = 1
local WALL_FRIEND = 2
--directions
local D_UP = 1
local D_LEFT = 2
local D_DOWN = 3
local D_RIGHT = 4
local D_UPLEFT = 5
local D_DOWNLEFT = 6
local D_DOWNRIGHT = 7
local D_UPRIGHT = 8

local function IsNearOceanIce(x, y, z)
    local tilex, tiley = TheWorld.Map:GetTileCoordsAtPoint(x, y, z)
    local cx, cy, cz = TheWorld.Map:GetTileCenterPoint(tilex, tiley)

    if TheWorld.Map:IsOceanIceAtPoint(cx - 8, cy, cz) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx + 8, cy, cz) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx, cy, cz - 8) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx, cy, cz + 8) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx - 8, cy, cz - 8) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx - 8, cy, cz + 8) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx + 8, cy, cz - 8) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx + 8, cy, cz + 8) then return true end

    return false
end

local function GetCellsWithAvailableWalls(inst, target)
    --roadmap cells (* - target)
    -- 5 1 8     x
    -- 2 * 4     |
    -- 6 3 7  z--+
    local cells = {}
    local x, y, z = target.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 1.5, {"wall"}, {"mh_lock", "mh_capture"})

    for _, wall in ipairs(ents) do
        if wall ~= nil and wall:IsValid() and wall.components.myhome_wall ~= nil then
           if (TUNING.MYHOME.ADMIN and inst._playerlink.Network:IsServerAdmin()) or
              (wall.components.myhome_wall.ownerindex == inst._userindex) or
              TheWorld.net.components.myhome_manager:IsFriend(wall.components.myhome_wall.ownerindex, inst._userindex) then
                local wx, _, wz = wall.Transform:GetWorldPosition()
                if wx > x  and wz == z then table.insert(cells, {D_UP, wall}) end
                if wx == x and wz > z  then table.insert(cells, {D_LEFT, wall}) end
                if wx < x  and wz == z then table.insert(cells, {D_DOWN, wall}) end
                if wx == x and wz < z  then table.insert(cells, {D_RIGHT, wall}) end
                if wx > x  and wz > z  then table.insert(cells, {D_UPLEFT, wall}) end
                if wx < x  and wz > z  then table.insert(cells, {D_DOWNLEFT, wall}) end
                if wx < x  and wz < z  then table.insert(cells, {D_DOWNRIGHT, wall}) end
                if wx > x  and wz < z  then table.insert(cells, {D_UPRIGHT, wall}) end
            end
        end
    end
    return cells
end

local function GetCornerWallFromCells(cells)
    local result = nil
    local num = 0
    local tpl = {}
    for _, v in ipairs(cells) do
        tpl[v[1]] = v[2]
    end
    if tpl[D_UP]    and tpl[D_UPLEFT]    and not tpl[D_UPRIGHT]   then result = tpl[D_UP]    num = num + 1 end
    if tpl[D_UP]    and tpl[D_UPRIGHT]   and not tpl[D_UPLEFT]    then result = tpl[D_UP]    num = num + 1 end
    if tpl[D_LEFT]  and tpl[D_UPLEFT]    and not tpl[D_DOWNLEFT]  then result = tpl[D_LEFT]  num = num + 1 end
    if tpl[D_LEFT]  and tpl[D_DOWNLEFT]  and not tpl[D_UPLEFT]    then result = tpl[D_LEFT]  num = num + 1 end
    if tpl[D_DOWN]  and tpl[D_DOWNLEFT]  and not tpl[D_DOWNRIGHT] then result = tpl[D_DOWN]  num = num + 1 end
    if tpl[D_DOWN]  and tpl[D_DOWNRIGHT] and not tpl[D_DOWNLEFT]  then result = tpl[D_DOWN]  num = num + 1 end
    if tpl[D_RIGHT] and tpl[D_DOWNRIGHT] and not tpl[D_UPRIGHT]   then result = tpl[D_RIGHT] num = num + 1 end
    if tpl[D_RIGHT] and tpl[D_UPRIGHT]   and not tpl[D_DOWNRIGHT] then result = tpl[D_RIGHT] num = num + 1 end
    if num > 1 then
        result = nil
    end
    return result
end

local function GetPolygonFromPoints(wallpoints)
    local polygon = {}
    local prevpt = nil
    local prevdir = nil
    for _, pt in ipairs(wallpoints) do
        if prevpt ~= nil then
            local dir = nil
            if pt[1] >  prevpt[1] and pt[2] == prevpt[2] then dir = D_UP end
            if pt[1] == prevpt[1] and pt[2] >  prevpt[2] then dir = D_LEFT end
            if pt[1] <  prevpt[1] and pt[2] == prevpt[2] then dir = D_DOWN end
            if pt[1] == prevpt[1] and pt[2] <  prevpt[2] then dir = D_RIGHT end
            if pt[1] >  prevpt[1] and pt[2] >  prevpt[2] then dir = D_UPLEFT end
            if pt[1] <  prevpt[1] and pt[2] >  prevpt[2] then dir = D_DOWNLEFT end
            if pt[1] <  prevpt[1] and pt[2] <  prevpt[2] then dir = D_DOWNRIGHT end
            if pt[1] >  prevpt[1] and pt[2] <  prevpt[2] then dir = D_UPRIGHT end
            if prevdir ~= dir then
                table.insert(polygon, prevpt)
            end
            prevdir = dir
        end
        prevpt = pt
    end
    return polygon
end

local function GetRectFromPolygon(polygon)
    local x1, z1, x2, z2 = polygon[1][1], polygon[1][2], polygon[1][1], polygon[1][2]
    for _, pt in ipairs(polygon) do
        if pt[1] > x2 then
            x2 = pt[1]
        elseif pt[1] < x1 then
            x1 = pt[1]
        end
        if pt[2] > z2 then
            z2 = pt[2]
        elseif pt[2] < z1 then
            z1 = pt[2]
        end
    end
    return {x1, z1, x2, z2}
end

local function GetCenterAndRadiusFromRect(rect)
    local x = (rect[1] + rect[3]) / 2
    local z = (rect[2] + rect[4]) / 2
    local r = math.abs(VecUtil_Dist(rect[1], rect[2], x, z)) + 0.5 -- +0.5 so that walls are inscribed in the circle
    return {x, z}, r
end

local function MarkerInsert(inst, icon, parent, pos)
    if icon ~= nil and parent ~= nil or pos ~= nil then
        table.insert(inst._markers, {icon = icon, parent = parent, pos = pos})
    end
end

local function UpdateInformer(inst)
    if inst._playerlink ~= nil and inst._playerlink.mh_informer ~= nil and inst._playerlink.mh_informer:IsValid() then
        inst._playerlink.mh_informer:SetMy(inst._typewalls.my)
        inst._playerlink.mh_informer:SetFriends(inst._typewalls.friends)
        inst._playerlink.mh_informer:SetAnothers(inst._typewalls.anothers)
    end
end

local function ClearGuides(inst)
    for _, v in ipairs(inst._dictguide) do
        if v:IsValid() and v.components.myhome_wall ~= nil then
            v:RemoveTag("mh_guide")
            v:RemoveTag("mh_capture")
            v:RemoveTag("mh_c"..inst._userindex)
            v.components.myhome_wall:SetModeColor(0)
            if v._guidefx ~= nil then
                v._guidefx:Remove()
                v._guidefx = nil
            end
        end
    end
    inst._dictguide = {}
end

local function WallInsertToGuide(inst, wall)
    wall:AddTag("mh_guide")
    wall:AddTag("mh_capture")
    wall:AddTag("mh_c"..inst._userindex)
    wall.components.myhome_wall:SetModeColor(1)
    table.insert(inst._dictguide, wall)
end

local function IsFirstWallNear(inst, x, z)
    local ents = TheSim:FindEntities(x, 0, z, 1.5, {"mh_first", "mh_capture"})
    for _, wall in ipairs(ents) do
        if wall:HasTag("mh_c"..inst._userindex) then
            return true
        end
    end
    return false
end

local function IsEntityInside(inst, ent)
    if ent and ent:IsValid() and ent.Transform and inst._polygon then
        local x, _, z = ent.Transform:GetWorldPosition()
        if TheSim:WorldPointInPoly(x, z, inst._polygon) then
            return true
        end
    end
    return false
end

local function SetModeColorAllFocusWalls(inst, mode)
    for _, v in ipairs(inst._dictfocus) do
        if v ~= nil and v:IsValid() and v.components.myhome_wall ~= nil then
            v.components.myhome_wall:SetModeColor(mode)
        end
    end
end

local function NotificationInsert(inst, text)
    local found = false
    for _, v in ipairs(inst._notifications) do
        if v == text then
            found = true
            break
        end
    end

    if not found then
        table.insert(inst._notifications, text)
    end
end

local function CancelTimeOutTask(inst)
    if inst._timeouttask ~= nil then
        inst._timeouttask:Cancel()
        inst._timeouttask = nil
    end
end

local function Destruct(inst)
    CancelTimeOutTask(inst)
    ClearGuides(inst)

    for _, v in ipairs(inst._dictfocus) do
        if v ~= nil and v:IsValid() and v.components.myhome_wall ~= nil then
            v:RemoveTag("mh_focus")
            v:RemoveTag("mh_capture")
            v:RemoveTag("mh_first")
            v:RemoveTag("mh_c"..inst._userindex)
            v.components.myhome_wall:SetModeColor(0)
        end
    end

    if inst._playerlink ~= nil then
        inst._playerlink:RemoveTag("mh_captor")
        if inst._playerlink.mh_informer ~= nil then
            inst._playerlink.mh_informer:DoTaskInTime(0, inst._playerlink.mh_informer.Remove)
            inst._playerlink.mh_informer = nil
        end
    end
    inst._playerlink = nil

    inst:DoTaskInTime(FRAMES, inst.Remove)
end

local function Timeout(inst)
    if inst._playerlink ~= nil and inst._playerlink:IsValid() and inst._playerlink.components.talker ~= nil then
        inst._playerlink.components.talker:Say(STRINGS.MYHOME.PROCESS_TIMEOUT, 3)
    end

    Destruct(inst)
end

local function Fail(inst)
    CancelTimeOutTask(inst)

    --pass flash 1
    SetModeColorAllFocusWalls(inst, 1)
    --pass flash 2
    inst:DoTaskInTime(0.5, function(inst)
        SetModeColorAllFocusWalls(inst, 2)
    end)

    inst:DoTaskInTime(1, function(inst)
        local text = table.concat(inst._notifications, "\n")
        if text == "" then
            text = STRINGS.MYHOME.PROCESS_FAIL
        end

        for _, v in ipairs(inst._markers) do
            if v.pos ~= nil then
                local fx = SpawnPrefab("myhome_marker")
                if fx ~= nil then
                    fx.Transform:SetPosition(v.pos.x, v.pos.y + 1.5, v.pos.z)
                    fx:SetIcon(v.icon)
                end
            elseif v.parent ~= nil and v.parent:IsValid() then
                local fx = SpawnPrefab("myhome_marker")
                if fx ~= nil then
                    fx.entity:SetParent(v.parent.entity)
                    fx.Transform:SetPosition(0, 1.5, 0)
                    fx:SetIcon(v.icon)
                end
            end
        end

        if inst._playerlink ~= nil and inst._playerlink:IsValid() and inst._playerlink.components.talker ~= nil then
            inst._playerlink.components.talker:Say(text, 8)
        end

        Destruct(inst)
    end)
end

local function Success(inst)
    local homeindex = TheWorld.net.components.myhome_manager:GenerateHomeID()
    for _, v in ipairs(inst._dictfocus) do
        if v ~= nil and v:IsValid() and v.components.myhome_wall ~= nil then
            if v:HasTag("mh_first") then
                v:AddTag("mh_ctrl")
            end
            v.components.myhome_wall:Lock(homeindex, inst._ownerindex)
            v.components.myhome_wall:SetModeColor(1) --pass flash 1
        end
    end

    if TUNING.MYHOME.PUSHOUT_HAUNTABLE_MOBS then
        for _, ent in ipairs(TheSim:FindEntities(inst._center[1], 0, inst._center[2], inst._radius, {"mh_tracked"})) do
            for _, v in ipairs(prefabdefs.hauntable_mobs_prefabs) do
                if ent.prefab == v and ent.prefab ~= "spat" and ent.components.myhome_trackable then
                    ent.components.myhome_trackable:Pause()
                end
            end
        end
    end

    --pass flash 2
    inst:DoTaskInTime(0.5, function(inst)
        SetModeColorAllFocusWalls(inst, 2)
    end)

    inst:DoTaskInTime(1, function(inst)
        local myhome = SpawnPrefab("myhome")
        if myhome ~= nil then
            myhome.Transform:SetPosition(inst._center[1], 0, inst._center[2])
            myhome:SetData({radius = inst._radius,
                            rect = inst._rect,
                            polygon = inst._polygon,
                            ownerindex = inst._ownerindex,
                            homeindex = homeindex})
            myhome:DoTaskInTime(0, function(ent)
                ent:DoShowCtrl()
            end)
        end

        if inst._playerlink ~= nil and inst._playerlink:IsValid() and inst._playerlink.components.talker ~= nil then
            inst._playerlink.components.talker:Say(STRINGS.MYHOME.PROCESS_SUCCESS, 3)
        end

        if TUNING.MYHOME.ANNOUNCE and inst._playerlink ~= nil and inst._playerlink:IsValid() then
            TheNet:Announce(string.format(STRINGS.MYHOME.ANNOUNCE_SUCCESS, inst._playerlink:GetDisplayName(), #inst._dictfocus))
        end

        Destruct(inst)
    end)
end

local function Verification(inst)
    CancelTimeOutTask(inst)

    local haserror = false

    for i, v in ipairs(inst._dictfocus) do
        if not v:IsValid() then
            haserror = true
            if inst._dictpoints[i] ~= nil then
                MarkerInsert(inst, 0, nil, Vector3(inst._dictpoints[i][1], 0, inst._dictpoints[i][2]))
            end
            break
        elseif v:HasTag("fire") then
            haserror = true
            NotificationInsert(inst, STRINGS.MYHOME.LOOP_INVALID)
            MarkerInsert(inst, 0, v)
        end
    end

    if not haserror then
        inst._polygon = GetPolygonFromPoints(inst._dictpoints)
        inst._rect = GetRectFromPolygon(inst._polygon)
        inst._center, inst._radius = GetCenterAndRadiusFromRect(inst._rect)
    end

    if not haserror and TUNING.MYHOME.ADMIN and inst._playerlink.Network:IsServerAdmin() then
        return Success(inst)
    end

    if not haserror and TUNING.MYHOME.RESTRICTED_AREA then
        local total = #inst._dictfocus
        for _, ent in ipairs(TheSim:FindEntities(inst._center[1], 0, inst._center[2], inst._radius, nil, nil, {"mh_r1", "mh_r2", "mh_r3", "mh_r4"})) do
            local skip = false

            if total > 60 and ent:HasTag("mh_r4") then
                skip = true
            end

            if not skip and IsEntityInside(inst, ent) then
                local icon = 0
                if ent:HasTag("mh_r1") then
                    icon = 1
                elseif ent:HasTag("mh_r2") then
                    icon = 2
                elseif ent:HasTag("mh_r3") then
                    icon = 3
                elseif ent:HasTag("mh_r4") then
                    icon = 4
                end

                if ent:HasTag("mh_r4") then
                    NotificationInsert(inst, string.format(STRINGS.MYHOME.INSIDE_HOME60_FORBIDDEN, tostring(ent.name)))
                else
                    NotificationInsert(inst, string.format(STRINGS.MYHOME.INSIDE_HOME_FORBIDDEN, tostring(ent.name)))
                end

                MarkerInsert(inst, icon, ent)
                haserror = true
            end
        end
    end

    if not haserror then
        local total = inst._typewalls.my + inst._typewalls.friends + inst._typewalls.anothers
        if (total * 0.5) < inst._typewalls.friends then
            NotificationInsert(inst, STRINGS.MYHOME.ERROR_50PERCENT_WALLS)
            haserror = true
        end
    end

    if haserror then
        Fail(inst)
    else
        Success(inst)
    end
end

local function ContinueWall(inst, wall)
    local result = false
    local resulttype = nil
    local ownerindex = wall.components.myhome_wall.ownerindex
    local x, y, z = wall.Transform:GetWorldPosition()

    if ownerindex > 0 and inst._userindex > 0 then
        if ownerindex == inst._userindex then
            result = true
            resulttype = WALL_MY
        elseif TheWorld.net.components.myhome_manager:IsFriend(ownerindex, inst._userindex) then
            result = true
            resulttype = WALL_FRIEND
        else
            resulttype = WALL_ANOTHER
        end

        --check walkable platform (boats)
        --regardless of the admin status (must be before next if "TUNING.MYHOME.ADMIN")
        if TheWorld.Map:GetPlatformAtPoint(x, y, z) then
            NotificationInsert(inst, STRINGS.MYHOME.WALKABLE_PLATFORM_FORBIDDEN)
            MarkerInsert(inst, 0, wall)
            inst:DoTaskInTime(0, Fail)
            inst._timeouttask = inst:DoTaskInTime(TIMEOUT, Timeout)
            return false, resulttype
        end

        --check ocean ice tile
        --regardless of the admin status (must be before next if "TUNING.MYHOME.ADMIN")
        local isoceantile = false
        if TheWorld.Map:IsOceanIceAtPoint(x, y, z) then
            isoceantile = true
        elseif TheWorld.Map:IsVisualGroundAtPoint(x, y, z) then
            if IsNearOceanIce(x, y, z) then
                isoceantile = true 
            end
        end
        if isoceantile then
            NotificationInsert(inst, STRINGS.MYHOME.ICE_TILE_FORBIDDEN)
            MarkerInsert(inst, 0, wall)
            inst:DoTaskInTime(0, Fail)
            inst._timeouttask = inst:DoTaskInTime(TIMEOUT, Timeout)
            return false, resulttype
        end

        if TUNING.MYHOME.ADMIN and inst._playerlink.Network:IsServerAdmin() then
            return true, resulttype
        end

        local total = #inst._dictfocus
        if TUNING.MYHOME.MAX_WALLS ~= 0 and total > TUNING.MYHOME.MAX_WALLS then
            NotificationInsert(inst, string.format(STRINGS.MYHOME.ERROR_MAX_WALLS, TUNING.MYHOME.MAX_WALLS))
            inst:DoTaskInTime(0, Fail)
            return false, resulttype
        end

        --check restrict 2, 3 (outside)
        if TUNING.MYHOME.RESTRICTED_AREA then
            for _, v in ipairs(TheSim:FindEntities(x, 0, z, 8, {"mh_r2"})) do
                NotificationInsert(inst, string.format(STRINGS.MYHOME.OUTSIDE_HOME_FORBIDDEN, tostring(v.name)))
                MarkerInsert(inst, 2, v)
                inst:DoTaskInTime(0, Fail)
                return false, resulttype
            end
            for _, v in ipairs(TheSim:FindEntities(x, 0, z, 16, {"mh_r3"})) do
                NotificationInsert(inst, string.format(STRINGS.MYHOME.OUTSIDE_HOME_FORBIDDEN, tostring(v.name)))
                MarkerInsert(inst, 3, v)
                inst:DoTaskInTime(0, Fail)
                return false, resulttype
            end
        end

        --check min distance any lock wall
        if TUNING.MYHOME.MIN_DIST_BETWEEN_MYHOMES > 0 then
            for _, v in ipairs(TheSim:FindEntities(x, 0, z, TUNING.MYHOME.MIN_DIST_BETWEEN_MYHOMES, {"mh_lock"})) do
                NotificationInsert(inst, STRINGS.MYHOME.INVALID_DISTANCE)
                MarkerInsert(inst, 0, wall)
                MarkerInsert(inst, 6, v)
                inst:DoTaskInTime(0, Fail)
                return false, resulttype
            end
        end

        --check distance not friendly lock wall
        for _, v in ipairs(TheSim:FindEntities(x, 0, z, 15, {"mh_lock"})) do
            if v.components.myhome_wall.ownerindex ~= inst._userindex and
               not TheWorld.net.components.myhome_manager:IsFriend(v.components.myhome_wall.ownerindex, inst._userindex) then
                NotificationInsert(inst, STRINGS.MYHOME.INVALID_DISTANCE)
                MarkerInsert(inst, 0, wall)
                MarkerInsert(inst, 6, v)
                inst:DoTaskInTime(0, Fail)
                return false, resulttype
            end
        end

        --check topology
        for i, node in ipairs(TheWorld.topology.nodes) do
            if TheSim:WorldPointInPoly(x, z, node.poly) then
                local id = TheWorld.topology.ids[i]
                if not TUNING.MYHOME.ACCESS_RUINS_AREA then
                    if id:find("TheLabyrinth") then
                        NotificationInsert(inst, STRINGS.MYHOME.AREA_LABYRINTH_FORBIDDEN)
                        MarkerInsert(inst, 0, wall)
                        inst:DoTaskInTime(0, Fail)
                        return false, resulttype
                    end
                    if id:find("Military") then
                        NotificationInsert(inst, STRINGS.MYHOME.AREA_RUINS_FORBIDDEN)
                        MarkerInsert(inst, 0, wall)
                        inst:DoTaskInTime(0, Fail)
                        return false, resulttype
                    end
                    if id:find("MilitaryPits") then
                        NotificationInsert(inst, STRINGS.MYHOME.AREA_RUINS_FORBIDDEN)
                        MarkerInsert(inst, 0, wall)
                        inst:DoTaskInTime(0, Fail)
                        return false, resulttype
                    end
                    if id:find("Sacred") then
                        NotificationInsert(inst, STRINGS.MYHOME.AREA_RUINS_FORBIDDEN)
                        MarkerInsert(inst, 0, wall)
                        inst:DoTaskInTime(0, Fail)
                        return false, resulttype
                    end
                    if id:find("SacredAltar") then
                        NotificationInsert(inst, STRINGS.MYHOME.AREA_RUINS_FORBIDDEN)
                        MarkerInsert(inst, 0, wall)
                        inst:DoTaskInTime(0, Fail)
                        return false, resulttype
                    end
                    if id:find("MoreAltars") then
                        NotificationInsert(inst, STRINGS.MYHOME.AREA_RUINS_FORBIDDEN)
                        MarkerInsert(inst, 0, wall)
                        inst:DoTaskInTime(0, Fail)
                        return false, resulttype
                    end
                    if id:find("AtriumMaze") then
                        NotificationInsert(inst, STRINGS.MYHOME.AREA_ATRIUM_FORBIDDEN)
                        MarkerInsert(inst, 0, wall)
                        inst:DoTaskInTime(0, Fail)
                        return false, resulttype
                    end
                end

                if not TUNING.MYHOME.ACCESS_HERMITCRAB_ISLAND and id:find("HermitcrabIsland") then
                    NotificationInsert(inst, STRINGS.MYHOME.AREA_HERMITCRABISLAND_FORBIDDEN)
                    MarkerInsert(inst, 0, wall)
                    inst:DoTaskInTime(0, Fail)
                    return false, resulttype
                end

                if not TUNING.MYHOME.ACCESS_ARCHIVE_MAZE then
                    if id:find("ArchiveMazeEntrance") then
                        NotificationInsert(inst, STRINGS.MYHOME.AREA_ARCHIVEMAZE_FORBIDDEN)
                        MarkerInsert(inst, 0, wall)
                        inst:DoTaskInTime(0, Fail)
                        return false, resulttype
                    end
                    if id:find("ArchiveMazeRooms") then
                        NotificationInsert(inst, STRINGS.MYHOME.AREA_ARCHIVEMAZE_FORBIDDEN)
                        MarkerInsert(inst, 0, wall)
                        inst:DoTaskInTime(0, Fail)
                        return false, resulttype
                    end
                end
            end
        end
    end

    if result == false then
        inst._timeouttask = inst:DoTaskInTime(TIMEOUT, Timeout)
    end

    return result, resulttype
end

--Note: with a lot number of walls, it's possible to get stack overflow
local function WallProcessing(inst, wall)
    if not wall:HasTag("mh_guide") or not wall:HasTag("mh_capture") or not wall:HasTag("mh_c"..inst._userindex) then
        return Destruct(inst)
    end

    CancelTimeOutTask(inst)
    ClearGuides(inst)

    wall:AddTag("mh_focus")
    wall:AddTag("mh_capture")
    wall:AddTag("mh_c"..inst._userindex)

    wall.components.myhome_wall:SetModeColor(2)

    local ownerindex = wall.components.myhome_wall.ownerindex
    local x, _, z = wall.Transform:GetWorldPosition()
    table.insert(inst._dictfocus, wall)
    table.insert(inst._dictpoints, {x, z})

    --Note: if continue == "false" then 
    --      timeouttask or Destruct and e.t.c logic must be sets in method ContinueWall
    local continue, typewall = ContinueWall(inst, wall)

    if typewall == WALL_MY then
        inst._typewalls.my = inst._typewalls.my + 1
    elseif typewall == WALL_FRIEND then
        inst._typewalls.friends = inst._typewalls.friends + 1
    elseif typewall == WALL_ANOTHER then
        inst._typewalls.anothers = inst._typewalls.anothers + 1
    end

    UpdateInformer(inst)

    if not continue then
        return --break stack
    end

    local cells = GetCellsWithAvailableWalls(inst, wall)
    local num = #cells
    if num == 0 then
        local total = #inst._dictfocus
        if total >= TUNING.MYHOME.MIN_WALLS or
           (TUNING.MYHOME.ADMIN and inst._playerlink.Network:IsServerAdmin() and total >= TUNING.MYHOME.MIN_WALLS_ADMIN) then
            if IsFirstWallNear(inst, x, z) then
                return Verification(inst)
            else
                NotificationInsert(inst, STRINGS.MYHOME.LOOP_NOT_FINISHED)
                MarkerInsert(inst, 5, wall)
            end
        elseif TUNING.MYHOME.ADMIN and inst._playerlink.Network:IsServerAdmin() then
            NotificationInsert(inst, string.format(STRINGS.MYHOME.ERROR_MIN_WALLS, TUNING.MYHOME.MIN_WALLS_ADMIN))
        else
            NotificationInsert(inst, string.format(STRINGS.MYHOME.ERROR_MIN_WALLS, TUNING.MYHOME.MIN_WALLS))
        end
        return Fail(inst)
    elseif num == 1 then
        local nextwall = cells[1][2]
        WallInsertToGuide(inst, nextwall)
        WallProcessing(inst, nextwall)
    else
        local cornerwall = GetCornerWallFromCells(cells)
        if cornerwall ~= nil then
            WallInsertToGuide(inst, cornerwall)
            WallProcessing(inst, cornerwall)
        else
            for _, v in ipairs(cells) do
                local guidewall = v[2]
                guidewall._guidefx = SpawnPrefab("myhome_guidemarker")
                if guidewall._guidefx ~= nil then
                     guidewall._guidefx.entity:SetParent(guidewall.entity)
                     guidewall._guidefx.Transform:SetPosition(0, 1.5, 0)
                end
                WallInsertToGuide(inst, guidewall)
            end
            inst._playerlink.components.talker:Say(STRINGS.MYHOME.SET_DIRECTION, 3)
            inst._timeouttask = inst:DoTaskInTime(TIMEOUT, Timeout)
        end
    end
end

local function OnFocusWall(inst, doer, wall)
    if doer == inst._playerlink and doer:HasTag("mh_captor") and 
       wall:HasTag("mh_guide") and wall:HasTag("mh_capture") and wall:HasTag("mh_c"..inst._userindex) then
        WallProcessing(inst, wall)
    end
end

local function LinkToPlayer(inst, player)
    inst._playerlink = player
    inst._userindex = TheWorld.net.components.myhome_manager:GetUserIndexByPlayer(player)

    inst:ListenForEvent("onremove", function() Destruct(inst) end, player)
    inst:ListenForEvent("death", function() Destruct(inst) end, player)
end

local function LinkToTarget(inst, target)
    inst._targetlink = target
    inst._dictguide = {}
    inst._dictfocus = {}
    inst._dictpoints = {}
    inst._notifications = {}
    inst._markers = {}
    inst._typewalls = {my = 0, friends = 0, anothers = 0}
end

local function OnInit(inst)
    if inst._playerlink == nil or inst._targetlink == nil or
       inst._targetlink.components.myhome_wall == nil or
       inst._targetlink.components.myhome_wall.ownerindex == 0 then
        inst:DoTaskInTime(0, inst.Remove)
        return
    end

    inst:ListenForEvent("myhome_focuswall", function(src, data)
        OnFocusWall(inst, data.doer, data.wall)
    end, TheWorld)

    WallInsertToGuide(inst, inst._targetlink)

    inst._targetlink:AddTag("mh_first")
    inst._playerlink:AddTag("mh_captor")
    inst._ownerindex = inst._targetlink.components.myhome_wall.ownerindex
    inst._playerlink.mh_informer = SpawnPrefab("myhome_informer")

    inst:DoTaskInTime(0, function(inst)
        if inst._playerlink.mh_informer ~= nil then
            inst._playerlink.mh_informer.entity:SetParent(inst._playerlink.entity)
            if TUNING.MYHOME.ADMIN and inst._playerlink.Network:IsServerAdmin() then
                inst._playerlink.mh_informer:SetAdmin()
            end
        end

        WallProcessing(inst, inst._targetlink)
    end)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:AddTransform()

    inst:AddTag("CLASSIFIED")
    inst:AddTag("NOCLICK")

    inst._playerlink = nil
    inst._targetlink = nil

    inst.LinkToPlayer = LinkToPlayer
    inst.LinkToTarget = LinkToTarget

    inst:DoTaskInTime(0, OnInit)

    return inst
end

return Prefab("myhome_builder", fn)
