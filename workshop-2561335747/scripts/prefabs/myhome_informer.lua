local function OnUpdateDirty(inst)
    if not TheNet:IsDedicated() then
        local total = inst._countmy:value() + inst._countfriends:value() + inst._countanothers:value()
        if inst._isadmin:value() then
            inst.label:SetText(string.format(STRINGS.MYHOME.INFORMER_ADMIN, total, inst._countmy:value(), inst._countfriends:value(), inst._countanothers:value()))
        else
            inst.label:SetText(string.format(STRINGS.MYHOME.INFORMER_DEFAULT, total, inst._countmy:value(), inst._countfriends:value()))
        end
    end
end

local function SetAdmin(inst)
    inst._isadmin:set(true)
    OnUpdateDirty(inst)
end

local function SetMy(inst, count)
    inst._countmy:set(count)
    OnUpdateDirty(inst)
end

local function SetFriends(inst, count)
    inst._countfriends:set(count)
    OnUpdateDirty(inst)
end

local function SetAnothers(inst, count)
    inst._countanothers:set(count)
    OnUpdateDirty(inst)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddNetwork()

    if not TheNet:IsDedicated() then
        inst.label = inst.entity:AddLabel()
        inst.label:SetFont(TALKINGFONT)
        inst.label:SetFontSize(30)
        inst.label:SetColour(1, 1, 1)
        inst.label:SetWorldOffset(0, 4, 0)
        inst.label:SetText("")
        inst.label:Enable(true)
    end

    inst._isadmin = net_bool(inst.GUID, "myhome_informer._isadmin", "isadmindirty")
    inst._countmy = net_ushortint(inst.GUID, "myhome_informer._countmy", "countmydirty")
    inst._countfriends = net_ushortint(inst.GUID, "myhome_informer._countfriends", "countfriendsdirty")
    inst._countanothers = net_ushortint(inst.GUID, "myhome_informer._countanothers", "countanothersdirty")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        inst:ListenForEvent("isadmindirty", OnUpdateDirty)
        inst:ListenForEvent("countmydirty", OnUpdateDirty)
        inst:ListenForEvent("countfriendsdirty", OnUpdateDirty)
        inst:ListenForEvent("countanothersdirty", OnUpdateDirty)
        return inst
    end

    inst.persists = false

    inst.SetAdmin = SetAdmin
    inst.SetMy = SetMy
    inst.SetFriends = SetFriends
    inst.SetAnothers = SetAnothers

    return inst
end

return Prefab("myhome_informer", fn)
