--Note: fake prefab, for "antlion_spawner"
--      without tag CLASSIFIED if restricted area mode
local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddNetwork()

    inst:AddTag("antlion_spawner_restricted")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    return inst
end

return Prefab("myhome_antlion_spawner", fn)
