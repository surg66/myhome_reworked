local Widget = require "widgets/widget"
local Text = require "widgets/text"
local TEMPLATES = require "widgets/redux/templates"
local UserCommands = require "usercommands"

local function GetRows()
    local players = {}
    local isdedicated = not TheNet:GetServerIsClientHosted()
    local myhome_manager = TheWorld.net.components.myhome_manager
    local userids = myhome_manager:GetUserIds()
    local names = myhome_manager:GetNames()
    --pass1
    local clients = TheNet:GetClientTable()
    if clients ~= nil then
        for _, v in ipairs(clients) do
            if not isdedicated or v.performance == nil then
                if v.userid ~= ThePlayer.userid then
                    local userindex = 0
                    for i, userid in ipairs(userids) do
                        if v.userid == userid then
                            userindex = i
                            break
                        end
                    end
                    table.insert(players, {userid = v.userid,
                                           name = v.name,
                                           userindex = userindex,
                                           inworld = false,
                                           online = true,
                                           isfriend = false,
                                           isfriendly = false,
                                           showuserid = false})
                end
            end
        end
    end
    --pass2
    for i, userid in ipairs(userids) do
        if userid ~= ThePlayer.userid then
            local found = false
            for _, player in ipairs(players) do
                if player.userid == userid then
                    found = true
                    break
                end
            end
            if not found then
                table.insert(players, {userid = userid,
                                       name = names[i],
                                       userindex = i,
                                       inworld = false,
                                       online = false,
                                       isfriend = false,
                                       isfriendly = false,
                                       showuserid = false})
            end
        end
    end
    --pass3
    if ThePlayer.myhome_userindex == nil then
        local userindex = myhome_manager:GetUserIndexFromTag(ThePlayer)
        if userindex > 0 then
            ThePlayer.myhome_userindex = userindex
        end
    end
    --pass4
    for i, player in ipairs(players) do
        if ThePlayer.myhome_userindex ~= nil and ThePlayer.myhome_userindex > 0 then
            if player.userindex > 0 then
                if myhome_manager:IsFriend(ThePlayer.myhome_userindex, player.userindex) then
                    players[i].isfriend = true
                end
                if myhome_manager:IsFriend(player.userindex, ThePlayer.myhome_userindex) then
                    players[i].isfriendly = true
                end
            end
        end
        if myhome_manager:InWorld(player.userid) then
            players[i].inworld = true
        else
            players[i].inworld = false
        end
        --chek dublicate names
        for _, p in ipairs(players) do
            if player.userid ~= p.userid and player.name == p.name then
                players[i].showuserid = true
                break
            end
        end
    end

    return players
end

local function SetTruncatedLeftJustifiedString(txt, str)
    txt:SetTruncatedString(str or "", txt._position.w, nil, true)
    local width, height = txt:GetRegionSize()
    txt:SetPosition(txt._position.x + width / 2, txt._position.y)
end

local function SetLeftJustifiedString(txt, str)
    txt:SetString(str)
    local width, height = txt:GetRegionSize()
    txt:SetPosition(txt._position.x + width / 2, txt._position.y)
end

local function SetRightJustifiedString(txt, str)
    txt:SetString(str)
    local width, height = txt:GetRegionSize()
    txt:SetPosition(txt._position.x - width / 2, txt._position.y)
end

local MyHomePlayersPage = Class(Widget, function(self, parent_screen)
    Widget._ctor(self, "MyHomePlayersPage")

    self.parent_screen = parent_screen
    self.last_time_update = 0

    self:CreatePlayers()

    if TheWorld ~= nil then
        self.inst:ListenForEvent("myhome_playerspagerefresh", function()
            --Note: once update in time
            local current_time = GetTime()
            if self.last_time_update ~= current_time then
                self.last_time_update = current_time
                --Note: need DoTaskInTime 0 to have time to get userindex from tag
                self.inst:DoTaskInTime(0, function()
                    self:OnPlayersUpdated()
                end)
            end
        end, TheWorld)
    end

    self:_DoFocusHookups()
end)

function MyHomePlayersPage:_DoFocusHookups()
    --lazy hack
    if TheInput:ControllerAttached() then
        self.parent_default_focus = self.players_list
        self.focus_forward = self.players_list
    else
        self.parent_default_focus = self.players_list.itemfocus
        self.focus_forward = self.players_list.itemfocus
    end
end

function MyHomePlayersPage:CreatePlayers()
    self.players_root = self:AddChild(Widget("players_root"))
    self.players_root:SetPosition(0, 0)

    self.none = self.players_root:AddChild(Text(CHATFONT, 22))
    self.none:SetColour(UICOLOURS.GOLD)
    self.num = self.players_root:AddChild(Text(CHATFONT, 20))
    self.num:SetColour(UICOLOURS.GOLD)

    self.players_list = self.players_root:AddChild(self:BuildPlayers())
    self.players_list:SetPosition(0, 0)
end

function MyHomePlayersPage:BuildPlayers()
    local num_rows = 7
    local row_height = 60
    local row_width = 750
    local spacing = 15
    local pos_left = -(row_width / 2)
    local pos_right = row_width / 2

    local function OnCommandWidget(widget)
        if widget.data.can_addfriend then
            UserCommands.RunUserCommand("myhomeaddfriend", {userid = widget.data.userid}, TheNet:GetClientTableForUser(TheNet:GetUserID()))
            return true
        end
        if widget.data.can_delfriend then
            UserCommands.RunUserCommand("myhomedelfriend", {userid = widget.data.userid}, TheNet:GetClientTableForUser(TheNet:GetUserID()))
            return true
        end
    end

    local function ScrollWidgetsCtor(context, index)
        local w = Widget("player-item-"..index)
        w.root = w:AddChild(Widget("root_player-item-"..index))
        w.bg = w.root:AddChild(TEMPLATES.ListItemBackground(row_width, row_height))
        w.bg:SetOnGainFocus(function() context.screen.players_list:OnWidgetFocus(w) end)

        w.name = w.root:AddChild(Text(CHATFONT, 25))
        w.name:SetColour(UICOLOURS.GOLD)
        w.name._position = {x = pos_left + spacing, y = 10, w = 550}

        w.online = w.root:AddChild(Text(CHATFONT, 22))
        w.online._position = {x = pos_left + spacing, y = -14}

        w.inworld = w.root:AddChild(Text(CHATFONT, 22))
        w.inworld:SetColour(WEBCOLOURS.SPRINGGREEN)
        w.inworld._position = {x = pos_left + spacing, y = -14}

        w.status = w.root:AddChild(Text(CHATFONT, 22))
        w.status._position = {x = pos_right - 50 - spacing, y = 0}

        w.checkbox = w.root:AddChild(TEMPLATES.StandardCheckbox(function() OnCommandWidget(w) end, 55, false))
        w.checkbox:SetPosition(pos_right - 20 - spacing, 0)

        if TheInput:ControllerAttached() then
            w.bg:UseFocusOverlay("serverlist_listitem_hover.tex")

            w.GetHelpText = function()
                local controller_id = TheInput:GetControllerID()
                local t = {}
                if TheInput:ControllerAttached() then
                    if w.data.can_addfriend then
                        table.insert(t, TheInput:GetLocalizedControl(controller_id, CONTROL_CONTROLLER_ACTION).." "..STRINGS.MYHOME.PAGE_PLAYERS_HELP_ADD)
                    end
                    if w.data.can_delfriend then
                        table.insert(t, TheInput:GetLocalizedControl(controller_id, CONTROL_CONTROLLER_ACTION).." "..STRINGS.MYHOME.PAGE_PLAYERS_HELP_DEL)
                    end
                end
                return table.concat(t, "  ")
            end

            w.OnControl = function(self, control, down)
                if context.screen._base.OnControl(self, control, down) then
                    return true
                end
                if TheInput:ControllerAttached() and not down then
                    if control == CONTROL_CONTROLLER_ACTION then
                        return OnCommandWidget(w)
                    end
                end
            end
        end

        w.focus_forward = w.bg

        return w
    end

    local function ScrollWidgetApply(context, widget, data, index)
        widget.data = data
        if data ~= nil then
            widget.root:Show()

            local name = ""
            if data.name then
                if data.showuserid then
                    name = data.name.." ("..data.userid..")"
                else
                    name = data.name
                end
            end

            SetTruncatedLeftJustifiedString(widget.name, name)
            SetLeftJustifiedString(widget.online, data.online and STRINGS.MYHOME.PAGE_PLAYERS_ONLINE or STRINGS.MYHOME.PAGE_PLAYERS_OFFLINE)
            SetLeftJustifiedString(widget.inworld, data.inworld and STRINGS.MYHOME.PAGE_PLAYERS_IN_WORLD or "")

            widget.online:Show()

            if data.online then
                widget.online:SetColour(WEBCOLOURS.LIGHTSKYBLUE)
            else
                widget.online:SetColour(WEBCOLOURS.SALMON)
            end

            if data.inworld then
                widget.inworld:Show()
                widget.online:Hide() --if in world then no need show online
            else
                widget.inworld:Hide()
            end

            widget.status:Show()

            local status = ""
            if data.isfriend and data.isfriendly then
                status = STRINGS.MYHOME.PAGE_PLAYERS_STATUS_FRIENDS
                widget.status:SetColour(WEBCOLOURS.SPRINGGREEN)
            elseif data.isfriend and not data.isfriendly then
                status = STRINGS.MYHOME.PAGE_PLAYERS_STATUS_MY_FRIEND
                widget.status:SetColour(WEBCOLOURS.YELLOW)
            elseif not data.isfriend and data.isfriendly then
                status = STRINGS.MYHOME.PAGE_PLAYERS_STATUS_FRIENDLY
                widget.status:SetColour(WEBCOLOURS.YELLOW)
            else
                widget.status:Hide()
            end

            if status ~= "" then
                SetRightJustifiedString(widget.status, status)
            end

            if data.isfriend then
                widget.data.can_addfriend = false
                widget.data.can_delfriend = true
                widget.checkbox:SetHoverText(STRINGS.MYHOME.PAGE_PLAYERS_HELP_DEL)
                widget.checkbox:SetTextures("images/global_redux.xml", "checkbox_normal_check.tex", "checkbox_focus_check.tex", "checkbox_normal.tex", nil, nil, {1,1}, {0,0})
            else
                widget.data.can_addfriend = true
                widget.data.can_delfriend = false
                widget.checkbox:SetHoverText(STRINGS.MYHOME.PAGE_PLAYERS_HELP_ADD)
                widget.checkbox:SetTextures("images/global_redux.xml", "checkbox_normal.tex", "checkbox_focus.tex", "checkbox_normal_check.tex", nil, nil, {1,1}, {0,0})
            end

            widget:Enable()
        else
            widget.root:Hide()
            widget:Disable()
        end
    end

    self.players = GetRows()

    local num_players = #self.players
    local num_friends = 0
    for _, v in ipairs(self.players) do
        if v.isfriend then
            num_friends = num_friends + 1
        end
    end
    local str_num = num_friends.."/"..tostring(num_players)

    self.none._position = {x = pos_left + spacing, y = 200}
    self.num._position = {x = pos_right - 15 - spacing, y = 230}

    SetLeftJustifiedString(self.none, STRINGS.MYHOME.PAGE_PLAYERS_NONE)
    SetRightJustifiedString(self.num, str_num)

    if num_players > 0 then
        self.none:Hide()
        self.num:Show()
    else
        self.none:Show()
        self.num:Hide()
    end

    local list = TEMPLATES.ScrollingGrid(self.players,
                                         {
                                            scroll_context = { screen = self },
                                            widget_width  = row_width,
                                            widget_height = row_height,
                                            num_visible_rows = num_rows,
                                            num_columns = 1,
                                            item_ctor_fn = ScrollWidgetsCtor,
                                            apply_fn = ScrollWidgetApply,
                                            scrollbar_offset = 20,
                                            scrollbar_height_offset = -60
                                         })
    list:SetItemsData(self.players)

    return list
end

function MyHomePlayersPage:OnPlayersUpdated()
    local scroll_index = 1
    local scroll_pos = nil
    if self.players_list ~= nil then
        scroll_index = self.players_list.focused_widget_index + self.players_list.displayed_start_index
        scroll_pos = self.players_list.current_scroll_pos
        self.players_list:Kill()
    end

    self.players_list = self.players_root:AddChild(self:BuildPlayers())
    self.players_list:SetPosition(0, 0)

    if scroll_pos ~= nil then
        self.players_list:ScrollToDataIndex(scroll_pos)
        self.players_list:ForceItemFocus(scroll_index)
    end

    self:_DoFocusHookups()
end

return MyHomePlayersPage
