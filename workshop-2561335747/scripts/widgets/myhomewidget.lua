local Widget = require "widgets/widget"
local ImageButton = require "widgets/imagebutton"
local TEMPLATES = require "widgets/redux/templates"
local PlayersPage = require "widgets/myhome_playerspage"
local InfoPage = require "widgets/myhome_infopage"

require("util")

local dialog_size_x = 830
local dialog_size_y = 495
local dialog_tabs_y = 290

local MyHomeWidget = Class(Widget, function(self, parent)
    Widget._ctor(self, "MyHomeWidget")

    self.root = self:AddChild(Widget("root"))

    local tab_root = self.root:AddChild(Widget("tab_root"))

    self.dialog = self.root:AddChild(TEMPLATES.RectangleWindow(dialog_size_x, dialog_size_y))
    self.dialog:SetPosition(0, 10)
    self.dialog.top:Hide()

    local base_size = 1

    local button_data = {
        {text = STRINGS.MYHOME.TAB_TITLE_PLAYERS, build_panel_fn = function() return PlayersPage(parent) end},
        {text = STRINGS.MYHOME.TAB_TITLE_INFO, build_panel_fn = function() return InfoPage(parent) end},
    }

    local function MakeTab(data, index)
        local tab = ImageButton("images/frontend_redux.xml", "list_tabs_normal.tex", nil,  nil, nil, "list_tabs_selected.tex")
        tab:SetFocusScale(base_size, base_size)
        tab:SetNormalScale(base_size, base_size)
        tab:SetText(data.text)
        tab:SetTextSize(22)
        tab:SetFont(HEADERFONT)
        tab:SetTextColour(UICOLOURS.GOLD)
        tab:SetTextFocusColour(UICOLOURS.HIGHLIGHT_GOLD)
        tab:SetTextSelectedColour(UICOLOURS.BLACK)
        tab.text:SetPosition(0, -2)
        tab.clickoffset = Vector3(0, 5, 0)
        tab:SetOnClick(function()
            self.last_selected:Unselect()
            self.last_selected = tab
            tab:Select()
            tab:MoveToFront()
            if self.panel ~= nil then
                self.panel:Kill()
            end
            self.panel = self.root:AddChild(data.build_panel_fn())
            self.focus_forward = self.panel.parent_default_focus

            if TheInput:ControllerAttached() then
                self.panel.parent_default_focus:SetFocus()
            end
        end)

        tab._tabindex = index - 1

        return tab
    end

    self.tabs = {}
    for i = 1, #button_data do
        table.insert(self.tabs, tab_root:AddChild(MakeTab(button_data[i], i)))
        self.tabs[#self.tabs]:SetPosition(0, 0)
        self.tabs[#self.tabs]:MoveToBack()
    end

    self:_PositionTabs(self.tabs, 220, dialog_tabs_y)

    local starting_tab = 1
    self.last_selected = self.tabs[starting_tab]
    self.last_selected:Select()
    self.last_selected:MoveToFront()
    self.panel = self.root:AddChild(button_data[starting_tab].build_panel_fn())

    self.focus_forward = self.panel.parent_default_focus
end)

function MyHomeWidget:_PositionTabs(tabs, width, y)
    local offset = #self.tabs / 2
    for i = 1, #self.tabs do
        local x = (i - offset - 0.5) * width
        tabs[i]:SetPosition(x, y)
    end
end

function MyHomeWidget:OnControlTabs(control, down)
    if control == CONTROL_OPEN_CRAFTING then
        local tab = self.tabs[((self.last_selected._tabindex - 1) % #self.tabs) + 1]
        if not down then
            tab.onclick()
            return true
        end
    elseif control == CONTROL_OPEN_INVENTORY then
        local tab = self.tabs[((self.last_selected._tabindex + 1) % #self.tabs) + 1]
        if not down then
            tab.onclick()
            return true
        end
    end
end

function MyHomeWidget:OnControl(control, down)
    if MyHomeWidget._base.OnControl(self, control, down) then return true end

    return self:OnControlTabs(control, down)
end

function MyHomeWidget:GetHelpText()
    local controller_id = TheInput:GetControllerID()
    local t = {}

    table.insert(t, TheInput:GetLocalizedControl(controller_id, CONTROL_OPEN_CRAFTING).."/"..TheInput:GetLocalizedControl(controller_id, CONTROL_OPEN_INVENTORY).." "..STRINGS.UI.HELP.CHANGE_TAB)

    return table.concat(t, "  ")
end

return MyHomeWidget
