local _G = GLOBAL
local ABIGAIL_DEFENSIVE_MAX_FOLLOW_DSQ = _G.TUNING.ABIGAIL_DEFENSIVE_MAX_FOLLOW * _G.TUNING.ABIGAIL_DEFENSIVE_MAX_FOLLOW

local function IsWithinDefensiveRange(inst)
    return inst._playerlink and inst:GetDistanceSqToInst(inst._playerlink) < ABIGAIL_DEFENSIVE_MAX_FOLLOW_DSQ
end

local function hometest(leader, target)
    if not leader or not target then return false end

    --pass 1, target inside myhome
    local x, y, z = target.Transform:GetWorldPosition()
    local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)
    if home ~= nil and not home:IsAvailableForDoer(leader) then 
        return true
    end

    --pass 2, target has home inside myhome
    if target.components.homeseeker and target.components.homeseeker:HasHome() then
        local x, y, z = target.components.homeseeker.home.Transform:GetWorldPosition()
        local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)
        if home ~= nil and not home:IsAvailableForDoer(leader) then
            return true
        end
    end

    return false
end

local function auratest(inst, target)
    if target == inst._playerlink then
        return false
    end

    if target.components.minigame_participator ~= nil then
        return false
    end

    if (target:HasTag("player") and not _G.TheNet:GetPVPEnabled()) or target:HasTag("ghost") or target:HasTag("noauradamage") then
        return false
    end

    local leader = inst.components.follower.leader
    if leader ~= nil
        and (leader == target
            or (target.components.follower ~= nil and
                target.components.follower.leader == leader)) then
        return false
    end

    if inst.is_defensive and not IsWithinDefensiveRange(inst) then
        return false
    end

    if inst.components.combat.target == target then
        if not hometest(inst._playerlink, target) then
            return true
        end
    end

    if target.components.combat.target ~= nil and
       (target.components.combat.target == inst or target.components.combat.target == leader) then
        if not hometest(inst._playerlink, target) then
            return true
        end
    end

    local ismonster = target:HasTag("monster")
    if ismonster and not _G.TheNet:GetPVPEnabled() and 
       ((target.components.follower and target.components.follower.leader ~= nil and 
         target.components.follower.leader:HasTag("player")) or target.bedazzled) then
        return false
    end
    
    if target:HasTag("companion") then
        return false
    end

    local result = ismonster or target:HasTag("prey")

    if hometest(inst._playerlink, target) then
        result = false
    end

    return result
end

AddPrefabPostInit("abigail", function(inst)
    if not _G.TheWorld.ismastersim then
        return inst
    end

    inst.components.combat:SetKeepTargetFunction(auratest)
    inst.components.aura.auratestfn = auratest
    inst.auratest = auratest
end)