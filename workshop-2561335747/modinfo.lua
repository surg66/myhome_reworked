name = "MyHome Reworked"
description = [[This mod will modify wall structures, by attributing each wall to its builder.
When any person right-clicks on any part of the wall, they may turn on conversion tool for "MyHome" program.
Afterwards, you may follow instructions to select the area to be labelled as "MyHome".
+ Walls are invincible.
+ Doors can only be opened to self and friends.
+ Can prevent griefing.
+ Push comming bosses, aggressive mobs outside home.
To add friends: press u, and type in #add(player id). (ie #add2)
To Cancel add friend: press u, and type in #del(player id). (ie #del2)]]

if locale == "zh" then
    name = "MyHome Reworked (私宅-重做)"
    description = [[该MOD将通过将每个墙体归属于其建造者来修改墙体结构。
当任何人使用鼠标右键点击自己建造的墙体任意部分时，就能打开“私宅”转变工具。
之后，你可以跟随指示来选择要标记为“私宅”的区域。
+私宅所使用的墙体将会无敌。
+ 门只能由自己或好友打开。
+ 可以防止糟心事。
+将袭击BOSS，袭击生物从家里赶出去。
添加好友：按U，输入 #add(玩家编号) 例如 #add2
删除好友：按U，输入 #del(玩家编号) 例如 #del2
按TAB键可在最左侧看到对应的玩家编号]]
end

author = "surg"
version = "1.4.1"
forumthread = ""
api_version_dst = 10
icon_atlas = "modicon.xml"
icon = "modicon.tex"
priority = 0
dont_starve_compatible = false
reign_of_giants_compatible = false
dst_compatible = true
all_clients_require_mod = true
client_only_mod = false
server_filter_tags = {"myhome"}

if locale == "zh" then
    --Simplified Chinese
    configuration_options =
    {
        {
            name = "language",
            label = "选择语言",
            hover = "选择你的语言",
            options =
            {
                {description = "English",    data = "en",  hover = "English"},
                {description = "简体中文",     data = "sch", hover = "Simplified Chinese"},
                {description = "Español",    data = "sp",  hover = "Spanish"},
                {description = "tiếng Việt", data = "vn",  hover = "VietNam"},
                {description = "Русский",    data = "ru",  hover = "Russian"},
            },
            default = "sch",
        },
        {
            name = "admin",
            label = "启用管理员权限",
            hover = "是否令管理员的私宅墙体数目及间距将不受限制",
            options =
            {
                {description = "开启", data = true, hover = "启用管理员权限"},
                {description = "关闭", data = false, hover = "禁用管理员权限"},
            },
            default = true,
        },
        {
            name = "announce",
            label = "公告",
            hover = "是否允许对转变及撤除私宅进行公告",
            options =
            {
                {description = "开启", data = true, hover = "允许对转变及撤除私宅进行公告"},
                {description = "关闭", data = false, hover = "禁止对转变及撤除私宅进行公告"},
            },
            default = true,
        },
        {
            name = "intruders",
            label = "入侵者",
            hover = "是否允许其他玩家进入私宅",
            options =
            {
                {description = "赶出去", data = true, hover = "禁止其他玩家进入私宅"},
                {description = "允许进入", data = false, hover = "允许其他玩家进入私宅"},
            },
            default = true,
        },
        {
            name = "min_walls",
            label = "墙体最小数目",
            hover = "设置私宅限制的墙体最小数目",
            options = (function()
                local values = {}
                for i = 1, 20 do
                    local val = 8 * i
                    values[i] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 16,
        },
        {
            name = "max_walls",
            label = "墙体最大数目",
            hover = "设置私宅限制的墙体最大数目",
            options = (function()
                local values = {{description = "无限制", data = 0}}
                for i = 1, 100 do
                    local val = 50 * i
                    values[i+1] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 300,
        },
        {
            name = "min_dist_between_myhomes",
            label = "私宅间最小间距",
            hover = "两所私宅间的最小间距",
            options =
            {
                {description = "无限制", data = 0},
                {description = "1块地皮", data = 5},
                {description = "2块地皮", data = 9},
            },
            default = 5,
        },
        {
            name = "restricted_area",
            label = "限制区域",
            hover = "是否启用限制区域",
            options =
            {
                {description = "开启", data = true, hover = "开启限制区域"},
                {description = "关闭", data = false, hover = "关闭限制区域"},
            },
            default = true,
        },
        {
            name = "robot_separate_areas_work",
            label = "为机器人启用单独的区域工作",
            hover = "单独的区域为机器人工作",
            options =
            {
                {description = "开启", data = true, hover = "开启"},
                {description = "关闭", data = false, hover = "关闭"},
            },
            default = true,
        },
        {
            name = "pushout_coming_bosses",
            label = "赶出袭击BOSS",
            hover = "是否在袭击BOSS生成在私宅中后，将它们赶出去",
            options =
            {
                {description = "开启", data = true, hover = "袭击BOSS生成在私宅中后，将它们赶出去"},
                {description = "关闭", data = false, hover = "袭击BOSS生成在私宅中后，不将它们赶出去"},
            },
            default = true,
        },
        {
            name = "pushout_coming_mobs",
            label = "赶出袭击生物",
            hover = "是否在袭击生物(猎犬、蠕虫，坎普斯等)生成在私宅中后，将它们赶出去",
            options =
            {
                {description = "开启", data = true, hover = "袭击怪物生成在私宅中后，将它们赶出去"},
                {description = "关闭", data = false, hover = "袭击怪物生成在私宅中后，不将它们赶出去"},
            },
            default = true,
        },
        {
            name = "pushout_hauntable_mobs",
            label = "赶出骚扰生物",
            hover = "是否在骚扰生物(考拉象、青蛙等)生成在私宅中后，将它们赶出去",
            options =
            {
                {description = "开启", data = true, hover = "骚扰生物生成在私宅中后，将它们赶出去"},
                {description = "关闭", data = false, hover = "骚扰生物生成在私宅中后，不将它们赶出去"},
            },
            default = true,
        },
        {
            name = "pushout_klaussack",
            label = "推出克劳斯赃物袋",
            hover = "是否在克劳斯的赃物袋生成在私宅中后，将它推出去",
            options =
            {
                {description = "开启", data = true, hover = "克劳斯的赃物袋生成在私宅中后，将它推出去"},
                {description = "关闭", data = false, hover = "克劳斯的赃物袋生成在私宅中后，不将它推出去"},
            },
            default = true,
        },
        {
            name = "force_pushout_wobot",
            label = "启用强制踢W.O.B.O.T",
            hover = "在家里强制踢W.O.B.O.T",
            options =
            {
                {description = "开启", data = true, hover = "开启"},
                {description = "关闭 (使用生命时间)", data = false, hover = "关闭"},
            },
            default = false,
        },
        {
            name = "access_ruins_area",
            label = "使用废墟区域",
            hover = "使用废墟区域,如果启用了【限制区域】",
            options =
            {
                {description = "开启", data = true, hover = "使用废墟区域"},
                {description = "关闭", data = false, hover = "不使用废墟区域"},
            },
            default = false,
        },
        {
            name = "access_hermitcrab_island",
            label = "使用寄居蟹隐士岛",
            hover = "使用寄居蟹隐士岛,如果启用了【限制区域】",
            options =
            {
                {description = "开启", data = true, hover = "使用寄居蟹隐士岛"},
                {description = "关闭", data = false, hover = "不使用寄居蟹隐士岛"},
            },
            default = false,
        },
        {
            name = "access_archive_maze",
            label = "使用档案馆区域",
            hover = "使用档案馆区域,如果启用了【限制区域】",
            options =
            {
                {description = "开启", data = true, hover = "使用档案馆区域"},
                {description = "关闭", data = false, hover = "不使用档案馆区域"},
            },
            default = false,
        },
        {
            name = "guest_glommer_days_alive",
            label = "Glommer在MyHome的生活时间。",
            hover = "Glommer在MyHome的生活时间。",
            options = (function()
                local values = {{description = "Disable", data = 0}}
                for i = 1, 10 do
                    local val = 10 * i
                    values[i+1] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 0,
        },
        {
            name = "guest_chester_days_alive",
            label = "Chester在MyHome的生活时间。",
            hover = "Chester在MyHome的生活时间。",
            options = (function()
                local values = {{description = "Disable", data = 0}}
                for i = 1, 10 do
                    local val = 10 * i
                    values[i+1] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 0,
        },
        {
            name = "guest_hutch_days_alive",
            label = "Hutch在MyHome的生活时间。",
            hover = "Hutch在MyHome的生活时间。",
            options = (function()
                local values = {{description = "Disable", data = 0}}
                for i = 1, 10 do
                    local val = 10 * i
                    values[i+1] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 0,
        },
        {
            name = "guest_wobot_days_alive",
            label = "WOBOT在MyHome的生活时间。",
            hover = "WOBOT在MyHome的生活时间。",
            options = (function()
                local values = {{description = "Disable", data = 0}}
                for i = 1, 10 do
                    local val = 10 * i
                    values[i+1] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 0,
        },
    }
else
    --default English
    configuration_options =
    {
        {
            name = "language",
            label = "Language",
            hover = "Default language.",
            options =
            {
                {description = "English",    data = "en",  hover = "English"},
                {description = "简体中文",     data = "sch", hover = "Simplified Chinese"},
                {description = "Español",    data = "sp",  hover = "Spanish"},
                {description = "tiếng Việt", data = "vn",  hover = "VietNam"},
                {description = "Русский",    data = "ru",  hover = "Russian"},
            },
            default = "en",
        },
        {
            name = "admin",
            label = "Enable admins",
            hover = "Admins is unrestricted",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off", data = false, hover = "disabled"},
            },
            default = true,
        },
        {
            name = "announce",
            label = "Enable announcements",
            hover = "Allow Announcements.",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off", data = false, hover = "disabled"},
            },
            default = true,
        },
        {
            name = "intruders",
            label = "Intruders",
            hover = "Intruder measures.",
            options =
            {
                {description = "get out", data = true, hover = "enable"},
                {description = "welcome", data = false, hover = "disabled"},
            },
            default = true,
        },
        {
            name = "min_walls",
            label = "Minimum needs wall",
            hover = "MyHome minimum needs walls.",
            options = (function()
                local values = {}
                for i = 1, 20 do
                    local val = 8 * i
                    values[i] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 16,
        },
        {
            name = "max_walls",
            label = "Maximum needs wall",
            hover = "MyHome maximum needs walls.",
            options = (function()
                local values = {{description = "unlimit", data = 0}}
                for i = 1, 100 do
                    local val = 50 * i
                    values[i+1] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 300,
        },
        {
            name = "min_dist_between_myhomes",
            label = "Minimum distance between homes",
            hover = "Minimum distance between homes.",
            options =
            {
                {description = "unlimit", data = 0},
                {description = "1 tile", data = 5},
                {description = "2 tiles", data = 9},
            },
            default = 5,
        },
        {
            name = "restricted_area",
            label = "Enable restricted area",
            hover = "Restricted area.",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off", data = false, hover = "disable"},
            },
            default = true,
        },
        {
            name = "robot_separate_areas_work",
            label = "Enable separate areas work for robot",
            hover = "Separate areas work for robot.",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off", data = false, hover = "disable"},
            },
            default = true,
        },
        {
            name = "pushout_coming_bosses",
            label = "Enable push out coming bosses",
            hover = "Push out bosses, after spawn inside home.",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off", data = false, hover = "disable"},
            },
            default = true,
        },
        {
            name = "pushout_coming_mobs",
            label = "Enable push out coming mobs",
            hover = "Push out aggressive mobs (hounds, worms, krampuses, etc), after spawn inside home.",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off", data = false, hover = "disable"},
            },
            default = true,
        },
        {
            name = "pushout_hauntable_mobs",
            label = "Enable push out hauntable mobs",
            hover = "Push out hauntable mobs (koalefants, wargs, etc), after spawn inside home.",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off", data = false, hover = "disable"},
            },
            default = true,
        },
        {
            name = "pushout_klaussack",
            label = "Enable push out klaus sack",
            hover = "Push out klaus sack, after spawn inside home.",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off", data = false, hover = "disable"},
            },
            default = true,
        },
        {
            name = "force_pushout_wobot",
            label = "Enable force push out W.O.B.O.T",
            hover = "Force push out W.O.B.O.T inside home.",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off (uses life time)", data = false, hover = "disable"},
            },
            default = false,
        },
        {
            name = "access_ruins_area",
            label = "Enable access ruins area",
            hover = "Access ruins area, if enable restricted area.",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off", data = false, hover = "disable"},
            },
            default = false,
        },
        {
            name = "access_hermitcrab_island",
            label = "Enable access hermit crab island",
            hover = "Access hermit crab island, if enable restricted area.",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off", data = false, hover = "disable"},
            },
            default = false,
        },
        {
            name = "access_archive_maze",
            label = "Enable access archive maze",
            hover = "Access archive maze, if enable restricted area.",
            options =
            {
                {description = "on", data = true, hover = "enable"},
                {description = "off", data = false, hover = "disable"},
            },
            default = false,
        },
        {
            name = "guest_glommer_days_alive",
            label = "Life time for Glommer inside MyHome.",
            hover = "Life time for Glommer inside MyHome.",
            options = (function()
                local values = {{description = "off", data = 0}}
                for i = 1, 10 do
                    local val = 10 * i
                    values[i+1] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 0,
        },
        {
            name = "guest_chester_days_alive",
            label = "Life time for Chester inside MyHome.",
            hover = "Life time for Chester inside MyHome.",
            options = (function()
                local values = {{description = "off", data = 0}}
                for i = 1, 10 do
                    local val = 10 * i
                    values[i+1] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 0,
        },
        {
            name = "guest_hutch_days_alive",
            label = "Life time for Hutch inside MyHome.",
            hover = "Life time for Hutch inside MyHome.",
            options = (function()
                local values = {{description = "off", data = 0}}
                for i = 1, 10 do
                    local val = 10 * i
                    values[i+1] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 0,
        },
        {
            name = "guest_wobot_days_alive",
            label = "Life time for WOBOT inside MyHome.",
            hover = "Life time for WOBOT inside MyHome.",
            options = (function()
                local values = {{description = "off", data = 0}}
                for i = 1, 10 do
                    local val = 10 * i
                    values[i+1] = {description = val.."", data = val}
                end
                return values
            end)(),
            default = 0,
        },
    }
end
